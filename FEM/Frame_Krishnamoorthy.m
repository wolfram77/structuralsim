function frame1
clear all
clc
fprintf('There are 3 types of elements \n 1:Column \n 2:Beam1 \n 3:Beam 2\n');
ndofn=3; % No of degree of freedom
%For column Type of element
Ac=30*50;
Lc=400;
Ec=2*10^3;
Ic=30*50^3/12;
kc=elestiff(Ac,Ec,Ic,Lc);
%for beam 1
Ab1=30*60;
Eb1=2*10^3;
Ib1=30*60^3/12;
Lb1=600;
kb1=elestiff(Ab1,Eb1,Ib1,Lb1);
%for beam 2
Ab2=30*60;
Eb2=2*10^3;
Ib2=30*60^3/12;
Lb2=400;
kb2=elestiff(Ab2,Eb2,Ib2,Lb2);
%transformation for column
T=[ 0 1 0  0 0 0 ;
   -1 0 0  0 0 0 ;
    0 0 1  0 0 0 ;
    0 0 0  0 1 0 ;
    0 0 0 -1 0 0 ;
    0 0 0  0 0 1];
%globalisation for column stffness elements with respect to horizontal axis
kcg=T'*kc*T;
%The no of elements 
nelem=30;
%The no of nodes 
node=21;
K=zeros(node*3,node*3);
for ielem=1:nelem
    %Assignment for each of 30 elements in the problem
    if (ielem>=1&ielem<=6)
        k=kcg;
        node1=(ielem-1)*3+1;
        node2=ielem*3+1;
    elseif(ielem>=7&ielem<=12)
         k=kcg;
        node1=(ielem-7)*3+2;
        node2=(ielem-6)*3+2;
      elseif(ielem>=13&ielem<=18)
         k=kcg;
        node1=(ielem-12)*3;
        node2=(ielem-11)*3;  
      elseif(ielem>=19&ielem<=24)
         k=kb1;
        node1=(ielem-18)*3+1;
        node2=(ielem-18)*3+2;  
    else
         k=kb2;
        node1=(ielem-24)*3+2;
        node2=(ielem-24)*3+3;
    end
    %Globalisation
     m=3*(node1-1)+1;
     n=3*(node2-1)+1;
     K1=zeros(node*3,node*3);
     K1(m:m+2,m:m+2)=k(1:3,1:3);
     K1(m:m+2,n:n+2)=k(1:3,4:6);
     K1(n:n+2,n:n+2)=k(4:6,4:6);
     K1(n:n+2,m:m+2)=k(4:6,1:3);
     K=K+K1;
end
 K  
rsdof=[1 2 3 4 5 6 7 8 9 ];
acdof=setdiff(1:node*ndofn,rsdof);
f=zeros(21*3,1);
%forces in the nodes
for ielem=19:nelem
    wtpme=.3;% wt in kN per cm in 
       if(ielem>=19&ielem<=24)
         k=kb1;
        node1=(ielem-18)*3+1;
        node2=(ielem-18)*3+2;
        lemem=600; % length of member in metre
        f(node1*3-2,1)=10;
        f(node1*3-1,1)=f(node1*3-1,1)-wtpme*lemem/2;
        f(node1*3,1)=f(node1*3,1)-wtpme*lemem^2/12;
        f(node2*3-1,1)=f(node2*3-1,1)-wtpme*lemem/2;
        f(node2*3,1)=f(node2*3,1)+wtpme*lemem^2/12;
       else
         k=kb2;
        node1=(ielem-24)*3+2;
        node2=(ielem-24)*3+3;
        lemem=400; % length of member in metre
        f(node1*3-1,1)=f(node1*3-1,1)-wtpme*lemem/2;
        f(node1*3,1)=f(node1*3,1)-wtpme*lemem^2/12;
        f(node2*3-1,1)=f(node2*3-1,1)-wtpme*lemem/2;
        f(node2*3,1)=f(node2*3,1)+wtpme*lemem^2/12;
       end
       
end
K
u=K(acdof,acdof)\f(acdof);
U=zeros(node*ndofn,1);
U(acdof)=u
F=K*U
F(acdof)=F(acdof)-f(acdof);
F
a=F(3*1)
a1=F(3*4)
b=F(3*2)
b1=F(3*5)
c=F(3*3)
c1=F(3*6)
d=F(3*4)
d1=F(3*5)
e=F(3*5)
e1=F(3*6)
% Wihdra of maximum moment in a member
for ielem=1:nelem
    %Assignment for each of 30 elements in the problem
    if (ielem>=1&ielem<=6)
        node1=(ielem-1)*3+1;
        node2=ielem*3+1;
    elseif(ielem>=7&ielem<=12)
        node1=(ielem-7)*3+2;
        node2=(ielem-6)*3+2;
      elseif(ielem>=13&ielem<=18)
        node1=(ielem-12)*3;
        node2=(ielem-11)*3;  
      elseif(ielem>=19&ielem<=24)
        node1=(ielem-18)*3+1;
        node2=(ielem-18)*3+2;  
    else
        node1=(ielem-24)*3+2;
        node2=(ielem-24)*3+3;
    end
    %For column
    if(ielem>=1&ielem<=18)
     m=3*(node1);
     n=3*(node2);
     mom1=F(m);
     mom2=F(n);
     if(mom1>mom2)
         fprintf('\nFor member %i     momxim end moment in kN.cm is %f\n',ielem,mom1)
     else
         fprintf('\nFor member %i     momxim end moment in kN.cm is %f\n',ielem,mom2)
     end
    elseif(ielem>=19&ielem<=24)
             m=3*(node1);
             n=3*(node2);
             mom1=F(m)%+.3*600^2/12;
             mom2=F(n)%-.3*600^2/12;
             if(mom1>mom2)
         fprintf('\nFor member %i     momxim end moment in kN.cm is %f and other is %f\n',ielem,mom1,mom2)
     else
         fprintf('\nFor member %i     momxim end moment in kN.cm is %f  and other is %f\n',ielem,mom2,mom2)
     end
    else
             m=3*(node1);
             n=3*(node2);
             mom1=F(m);
             mom2=F(n);
             if(mom1>mom2)
         fprintf('\nFor member %i     momxim end moment in kN.cm is %f\n',ielem,mom1)
     else
         fprintf('\nFor member %i     momxim end moment in kN.cm is %f\n',ielem,mom2)
     end
    end
end
 
 
function y=elestiff(A,E,I,L)
%It returns the element stiffness matrix of the element
y=[A*E/L     0            0           -A*E/L       0                  0   ;
      0       12*E*I/L^3     6*E*I/L^2     0          -12*E*I/L^3     6*E*I/L^2;
      0       6*E*I/L^2      4*E*I/L       0          -6*E*I/L^2      2*E*I/L   ;
      -A*E/L    0             0           A*E/L       0                  0   ;
      0       -12*E*I/L^3    -6*E*I/L^2     0          12*E*I/L^3     -6*E*I/L^2;
       0       6*E*I/L^2      2*E*I/L       0          -6*E*I/L^2      4*E*I/L   ];
   

