clear all;
clc;
syms s
n=2;
K=zeros(6);
D=400;
w=15;
gdof=[1 2; 2 3;3 4];
    A=[1 -1 1 -1; 0 1 -2 3; 1 1 1 1; 0 1 2 3];
    C=inv(A);
for ielem=1:n
    L=[4 4 ];
    N=[1 s s^2 s^3]*C;
    N(2)=N(2)*(L(ielem)/2);
    N(4)=N(4)*(L(ielem)/2);
    dN=diff(N,s,2);
    B=(4/(L(ielem)^2))*dN;
    k=zeros(4);
    Bt=transpose(B);
    V=Bt*B;
    M=(subs(V,s,-1/sqrt(3))+subs(V,s,1/sqrt(3)))*(L(ielem)/2);
    k=k+(M*D)
    kelm(:,:,ielem)=k;
    eldof=gdof(ielem,:);
    gldof=[(eldof(1)-1)*2+1 (eldof(1)-1)*2+2 (eldof(2)-1)*2+1 (eldof(2)-1)*2+2];
    K(gldof,gldof)=K(gldof,gldof)+k;  
end
K
K(3,3)=K(3,3)+((24*D)/4^3);
rdof=[1 2 5];
tdof=[1:6];
udof=setdiff(tdof,rdof);
K1=K(udof,udof)
 f=[-60;0;20]
 u=K1\f
 U=[0;0;u(1:2);0;u(3)]
 F=K*U
 u1=[1 2 3 4; 3 4 5 6];
for ielem=1:n
    disp=U(u1(ielem,:));
    m=kelm(:,:,ielem);
    p=[(w*L(ielem)/2);(w*L(ielem)^2)/12;(w*L(ielem)/2);-(w*L(ielem)^2)/12];
    sigma=(m*disp)+p
end
