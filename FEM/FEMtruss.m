clc;
clear all;
E=200;
A=2000;
L=[4000 sqrt(3000^2+4000^2) 3000];
theta=[0; atan(3/4);90*pi/180];
gdof=[1 2;1 3; 2 3];
nelem=3;
K=zeros(6);
for ielem=1:nelem
    C=cos(theta(ielem));
    S=sin(theta(ielem));
    k=A*E/L(ielem)*[C^2 C*S -C^2 -C*S;
                    C*S S^2 -C*S -S^2;
                    -C^2 -C*S C^2 C*S; 
                    -C*S -S^2 C*S S^2];
    eldof=gdof(ielem,:);
   gldof=[(eldof(1)-1)*2+1 (eldof(1)-1)*2+2 (eldof(2)-1)*2+1 (eldof(2)-1)*2+2];
   K(gldof,gldof)=K(gldof,gldof)+k;
end
K
rdof=[1 2 3 4];
tdof=[1:6];
udof=setdiff(tdof,rdof);
k1=K(udof,udof);
f=[15.0;20.0];
u=k1\f
U=[0;0;0;0;u(1:2)]
F=K*U
u1=[1 2 3 4; 1 2 5 6; 3 4 5 6];
for ielem=1:nelem
    disp=U(u1(ielem,:));
    d(ielem)=[-cos(theta(ielem)) -sin(theta(ielem)) cos(theta(ielem)) sin(theta(ielem))]*disp;
    sigma(ielem)=d(ielem)*E/L(ielem);
    P(ielem)=sigma(ielem)*A;
end
sigma
P