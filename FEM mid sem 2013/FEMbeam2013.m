clear all;
clc;
syms s
n=3;
K=zeros(8);
D=[1000 2000 2000];
w=12;% udl in first and 3rd spans
ptld=36;% pt load in 2nd span
pta=2;ptb=4;% pt load position
gdof=[1 2; 2 3;3 4];
    A=[1 -1 1 -1; 0 1 -2 3; 1 1 1 1; 0 1 2 3];
    C=inv(A);
    L=[4 6 6];
for ielem=1:n
    N=[1 s s^2 s^3]*C;
    N(2)=N(2)*(L(ielem)/2);
    N(4)=N(4)*(L(ielem)/2);
    dN=diff(N,s,2);
    B=(4/(L(ielem)^2))*dN;
    k=zeros(4);
    Bt=transpose(B);
    V=Bt*B;
    M=(subs(V,s,-1/sqrt(3))+subs(V,s,1/sqrt(3)))*(L(ielem)/2);
    k=k+(M*D(ielem));
    kelm(:,:,ielem)=k
    eldof=gdof(ielem,:);
    gldof=[(eldof(1)-1)*2+1 (eldof(1)-1)*2+2 (eldof(2)-1)*2+1 (eldof(2)-1)*2+2];
    K(gldof,gldof)=K(gldof,gldof)+k;  
end
K
rdof=[1 2 3 5 7];
tdof=[1:8];
udof=setdiff(tdof,rdof);
K1=K(udof,udof)
 f=[16;20;-36]% give loads in the unrestrained dofs
 u=K1\f;
 U=[0;0;0;u(1);0;u(2);0;u(3)]
 F=K*U
 u1=[1 2 3 4; 3 4 5 6; 5 6 7 8];
for ielem=1:n
    disp=U(u1(ielem,:));
    m=kelm(:,:,ielem);
    if ielem==2% it is point load for particular problem
    p=[(ptld*ptb)/L(ielem);(ptld*pta*ptb^2)/L(ielem)^2;(ptld*pta)/L(ielem);-(ptld*pta^2*ptb)/L(ielem)^2];
    else % they have udls in this problem, otherwise change
    p=[(w*L(ielem)/2);(w*L(ielem)^2)/12;(w*L(ielem)/2);-(w*L(ielem)^2)/12];
    end
    sigma=(m*disp)-p
end
