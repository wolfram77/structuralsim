clc;
clear all;
E=200;
A=250;
L=[750 750 1000];
theta=[53.13;126.87;143.14]*pi/180;
gdof=[1 4;1 3; 1 2];
nelem=3;
K=zeros(8);
for ielem=1:nelem
    C=cos(theta(ielem));
    S=sin(theta(ielem));
    k=A*E/L(ielem)*[C^2 C*S -C^2 -C*S;
                    C*S S^2 -C*S -S^2;
                    -C^2 -C*S C^2 C*S; 
                    -C*S -S^2 C*S S^2]
    eldof=gdof(ielem,:);
   gldof=[(eldof(1)-1)*2+1 (eldof(1)-1)*2+2 (eldof(2)-1)*2+1 (eldof(2)-1)*2+2];
   K(gldof,gldof)=K(gldof,gldof)+k;
end
K
rdof=[3 4 5 6 7 8];
tdof=[1:8];
udof=setdiff(tdof,rdof);
k1=K(udof,udof);
f=[0; -18];
u=k1\f
U=[u(1:2);0;0;0;0;0;0]
F=K*U
u1=[1 2 7 8; 1 2 5 6; 1 2 3 4];
for ielem=1:nelem
    disp=U(u1(ielem,:));
    d(ielem)=[-cos(theta(ielem)) -sin(theta(ielem)) cos(theta(ielem)) sin(theta(ielem))]*disp;
    sigma(ielem)=d(ielem)*E/L(ielem);
    P(ielem)=sigma(ielem)*A;
end
sigma
P