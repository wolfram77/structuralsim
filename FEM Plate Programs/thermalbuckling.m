clc
nx=4;ny=4;t=.01;L=1;H=1;
theta(1:4)=[0 pi/2 pi/2 0];
nop = 4;
E1=130*10^9;
E2at300=9.5*10^9;
E2=9.5*10^9;
u12=.3;
G12=6.0*10^9;
G23=3.0*10^9;
alpha1=-.3*10^-6;
alpha2=28.1*10^-6;
beta1=0;
beta2=0.44;
TEMP=0.00;
MOIS=0.001;
u21=u12*E2/E1;
G13=G12;
T=zeros(2*ny+1,2*nx+1);
c=1;
for i=1:2*ny+1
    for j=1:2*nx+1;
        if  mod(i,2)==0
            if mod(j,2)==0
                T(i,j)=0;
            else
                T(i,j)=c;
                c=c+1;        
            end
        else
            T(i,j)=c;
            c=c+1;        
        end
    end
end
top=t/nop;
z2=-t/2:t/nop:t/2;
D1=zeros(3,3);D2=zeros(2,2);D3=zeros(3,3);D4=zeros(3,3);
for i=1:nop
    ornt=theta(i);
    TF1=[(cos(ornt))^2 (sin(ornt))^2 sin(ornt)*cos(ornt);(sin(ornt))^2 (cos(ornt))^2 -sin(ornt)*cos(ornt);-2*sin(ornt)*cos(ornt) 2*sin(ornt)*cos(ornt) (cos(ornt))^2-(sin(ornt))^2];
    TF2=[cos(ornt) sin(ornt);-sin(ornt) cos(ornt)];
    D1a=TF1'*[E1/(1-u12*u21) E2*u12/(1-u12*u21) 0;E2*u12/(1-u12*u21) E2/(1-u12*u21) 0;0 0 G12]*TF1;
    D2a=TF2'*[G13 0;0 G23]*TF2;
    D3a=D1a;
    D4a=D1a;
    D1=D1+D1a*top;
    D2=D2+D2a*((z2(i+1)-z2(i))-4*(z2(i+1)^3-z2(i)^3)/(3*t^2));
    D3=D3+D3a*(z2(i+1)^3-z2(i)^3)/3;
    D4=D4+D4a*(z2(i+1)^2-z2(i)^2)/2;
end
D=[D1 zeros(3,2) D4;zeros(2,3) D2 zeros(2,3);D4 zeros(3,2) D3];
Nn=zeros(3,1);Mn=zeros(3,1);
for i=1:nop
    ornt=theta(i);
    TF3=[(cos(ornt))^2 (sin(ornt))^2;(sin(ornt))^2 (cos(ornt))^2;2*sin(ornt)*cos(ornt) -2*sin(ornt)*cos(ornt)];
    TF1=[(cos(ornt))^2 (sin(ornt))^2 sin(ornt)*cos(ornt);(sin(ornt))^2 (cos(ornt))^2 -sin(ornt)*cos(ornt);-2*sin(ornt)*cos(ornt) 2*sin(ornt)*cos(ornt) (cos(ornt))^2-(sin(ornt))^2];
    Qij=TF1'*[E1/(1-u12*u21) E2*u12/(1-u12*u21) 0;E2*u12/(1-u12*u21) E2/(1-u12*u21) 0;0 0 G12]*TF1;        
    Nn=Nn+Qij*TF3*[alpha1;alpha2]*TEMP*(z2(i+1)-z2(i))+Qij*TF3*[beta1;beta2]*MOIS*(z2(i+1)-z2(i));
    Mn=Mn+Qij*TF3*[alpha1;alpha2]*TEMP*(z2(i+1)^2-z2(i)^2)/2+Qij*TF3*[beta1;beta2]*MOIS*(z2(i+1)^2-z2(i)^2)/2;
end
Fn=-[Nn;0;0;Mn];
I=[t 0 0 0 0;0 t 0 0 0;0 0 t 0 0;0 0 0 (t^3)/12 0;0 0 0 0 (t^3)/12];
X=zeros(1,2*nx+1);
for i=1:2*nx+1
    X(1,i)=(L/(2*nx))*(i-1);
end
Y=zeros(2*ny+1,1);
for i=1:2*ny+1
    Y(i,1)=(H/(2*ny))*(i-1);
end
n=T(2*ny+1,2*nx+1);
K=zeros(5*n,5*n);
F=zeros(5*n,1);
M=zeros(5*n,5*n);
for i=1:2:2*ny-1
    for j=1:2:2*nx-1
        na=T(i,j);
        nb=T(i,j+1);
        nc=T(i,j+2);
        nd=T(i+1,j+2);
        ne=T(i+2,j+2);
        nf=T(i+2,j+1);
        ng=T(i+2,j);
        nh=T(i+1,j);
        xa=X(1,j);
        xb=X(1,j+1);
        xc=X(1,j+2);
        xd=X(1,j+2);
        xe=X(1,j+2);
        xf=X(1,j+1);
        xg=X(1,j);
        xh=X(1,j);
        ya=Y(i,1);
        yb=Y(i,1);
        yc=Y(i,1);
        yd=Y(i+1,1);
        ye=Y(i+2,1);
        yf=Y(i+2,1);
        yg=Y(i+2,1);
        yh=Y(i+1,1);
        Ke=zeros(40,40);
        Fe=zeros(40,1);
        Me=zeros(40,40);
        for q=-1/sqrt(3):2/sqrt(3):1/sqrt(3)
            for p=-1/sqrt(3):2/sqrt(3):1/sqrt(3)
                r=q;
                s=p;
       N1=(r-1)*(1-s)*(r+s+1)/4;
       N2=(1-r^2)*(1-s)/2;
       N3=(1+r)*(1-s)*(r-s-1)/4;
       N4=(1+r)*(1-s^2)/2;
       N5=(1+r)*(1+s)*(r+s-1)/4;
       N6=(1-r^2)*(1+s)/2;
       N7=(r-1)*(1+s)*(r-s+1)/4;
       N8=(1-r)*(1-s^2)/2;
       N1r=1/4*(1-s)*(r+s+1)+1/4*(r-1)*(1-s);
       N1s=-1/4*(r-1)*(r+s+1)+1/4*(r-1)*(1-s);
       N2r=-r*(1-s);
       N2s=-1/2+1/2*r^2;
       N3r=1/4*(1-s)*(r-s-1)+1/4*(1+r)*(1-s);
       N3s=-1/4*(1+r)*(r-s-1)-1/4*(1+r)*(1-s);
       N4r=1/2-1/2*s^2;
       N4s=-(1+r)*s;
       N5r=1/4*(1+s)*(r+s-1)+1/4*(1+r)*(1+s);
       N5s=1/4*(1+r)*(r+s-1)+1/4*(1+r)*(1+s);
       N6r=-r*(1+s);
       N6s=1/2-1/2*r^2;
       N7r=1/4*(1+s)*(r-s+1)+1/4*(r-1)*(1+s);
       N7s=1/4*(r-1)*(r-s+1)-1/4*(r-1)*(1+s);
       N8r=-1/2+1/2*s^2;
       N8s=-(1-r)*s;
       N=[N1 N2 N3 N4 N5 N6 N7 N8 zeros(1,32);zeros(1,8) N1 N2 N3 N4 N5 N6 N7 N8 zeros(1,24);zeros(1,16) N1 N2 N3 N4 N5 N6 N7 N8 zeros(1,16);zeros(1,24) N1 N2 N3 N4 N5 N6 N7 N8 zeros(1,8);zeros(1,32) N1 N2 N3 N4 N5 N6 N7 N8];
        J=[N1r N2r N3r N4r N5r N6r N7r N8r;N1s N2s N3s N4s N5s N6s N7s N8s]*[xa ya;xb yb;xc yc;xd yd;xe ye;xf yf;xg yg;xh yh];
        G1=inv(J);
        G=[G1(1,1:2) 0 0;0 0 G1(2,1:2);G1(2,1:2) G1(1,1:2)];
        P=[N1r N2r N3r N4r N5r N6r N7r N8r 0 0 0 0 0 0 0 0;N1s N2s N3s N4s N5s N6s N7s N8s 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 N1r N2r N3r N4r N5r N6r N7r N8r;0 0 0 0 0 0 0 0 N1s N2s N3s N4s N5s N6s N7s N8s];
        B1=G*P;
        B2a=G1*[N1r N2r N3r N4r N5r N6r N7r N8r zeros(1,16);N1s N2s N3s N4s N5s N6s N7s N8s zeros(1,16)];
        B2b=[zeros(1,8) N1 N2 N3 N4 N5 N6 N7 N8 zeros(1,8);zeros(1,16) N1 N2 N3 N4 N5 N6 N7 N8];
        B2=B2a-B2b;
        B3=B1;
        B=[B1 zeros(3,24);zeros(2,16) B2;zeros(3,24) B3];
        keb=B'*D*B*det(J);
        Ke=Ke+keb;
        fe=B'*Fn*det(J);
        Fe=Fe+fe;
        meb=N'*I*N*det(J);
        Me=Me+meb;
            end
        end
A=[na nb nc nd ne nf ng nh n+na n+nb n+nc n+nd n+ne n+nf n+ng n+nh 2*n+na 2*n+nb 2*n+nc 2*n+nd 2*n+ne 2*n+nf 2*n+ng 2*n+nh 3*n+na 3*n+nb 3*n+nc 3*n+nd 3*n+ne 3*n+nf 3*n+ng 3*n+nh 4*n+na 4*n+nb 4*n+nc 4*n+nd 4*n+ne 4*n+nf 4*n+ng 4*n+nh];
for q=1:40
    for p=1:40
        aq=A(q);
        ap=A(p);
        K(aq,ap)=K(aq,ap)+Ke(q,p);
        M(aq,ap)=M(aq,ap)+Me(q,p);
    end
end
for p=1:40
    ap=A(p);
    F(ap,1)=F(ap,1)+Fe(p,1);
end
    end
end
k1=K;
p1=(4*nx)+(4*ny);
T1=T(2:(2*ny+1),(2*nx+1));
T2=T(2:(2*ny),1);
A1=[T(1,1:(2*nx+1)) T1' T((2*ny+1),1:(2*nx)) T2'];
for i=1:p1
    df=A1(1,i);    
    k1(df,df)=k1(df,df)*(10^6);
    k1(df+n,df+n)=k1(df+n,df+n)*(10^6);
    k1(df+2*n,df+2*n)=k1(df+2*n,df+2*n)*(10^6);
    for j=1:2*nx+1
        if(df==T(1,j))
            k1(df+3*n,df+3*n)=k1(df+3*n,df+3*n)*(10^6);
        end
        if(df==T(2*ny+1,j))
            k1(df+3*n,df+3*n)=k1(df+3*n,df+3*n)*(10^6);
        end
    end
    for j=1:2*ny+1
        if(df==T(j,1))
            k1(df+4*n,df+4*n)=k1(df+4*n,df+4*n)*(10^6);
        end
        if(df==T(j,2*nx+1))
            k1(df+4*n,df+4*n)=k1(df+4*n,df+4*n)*(10^6);
        end
    end
end
disp=k1\F;
Kgr=zeros(5*n,5*n);
for i=1:2:2*ny-1
    for j=1:2:2*nx-1
        na=T(i,j);
        nb=T(i,j+1);
        nc=T(i,j+2);
        nd=T(i+1,j+2);
        ne=T(i+2,j+2);
        nf=T(i+2,j+1);
        ng=T(i+2,j);
        nh=T(i+1,j);
        dispe=[disp(na);disp(nb);disp(nc);disp(nd);disp(ne);disp(nf);disp(ng);disp(nh);disp(n+na);disp(n+nb);disp(n+nc);disp(n+nd);disp(n+ne);disp(n+nf);disp(n+ng);disp(n+nh);disp(2*n+na);disp(2*n+nb);disp(2*n+nc);disp(2*n+nd);disp(2*n+ne);disp(2*n+nf);disp(2*n+ng);disp(2*n+nh);disp(3*n+na);disp(3*n+nb);disp(3*n+nc);disp(3*n+nd);disp(3*n+ne);disp(3*n+nf);disp(3*n+ng);disp(3*n+nh);disp(4*n+na);disp(4*n+nb);disp(4*n+nc);disp(4*n+nd);disp(4*n+ne);disp(4*n+nf);disp(4*n+ng);disp(4*n+nh)];
        xa=X(1,j);
        xb=X(1,j+1);
        xc=X(1,j+2);
        xd=X(1,j+2);
        xe=X(1,j+2);
        xf=X(1,j+1);
        xg=X(1,j);
        xh=X(1,j);
        ya=Y(i,1);
        yb=Y(i,1);
        yc=Y(i,1);
        yd=Y(i+1,1);
        ye=Y(i+2,1);
        yf=Y(i+2,1);
        yg=Y(i+2,1);
        yh=Y(i+1,1);
        Kger=zeros(40,40);
        for q=-1/sqrt(3):2/sqrt(3):1/sqrt(3)
            for p=-1/sqrt(3):2/sqrt(3):1/sqrt(3)
                r=q;
                s=p;
       N1=(r-1)*(1-s)*(r+s+1)/4;
       N2=(1-r^2)*(1-s)/2;
       N3=(1+r)*(1-s)*(r-s-1)/4;
       N4=(1+r)*(1-s^2)/2;
       N5=(1+r)*(1+s)*(r+s-1)/4;
       N6=(1-r^2)*(1+s)/2;
       N7=(r-1)*(1+s)*(r-s+1)/4;
       N8=(1-r)*(1-s^2)/2;
       N1r=1/4*(1-s)*(r+s+1)+1/4*(r-1)*(1-s);
       N1s=-1/4*(r-1)*(r+s+1)+1/4*(r-1)*(1-s);
       N2r=-r*(1-s);
       N2s=-1/2+1/2*r^2;
       N3r=1/4*(1-s)*(r-s-1)+1/4*(1+r)*(1-s);
       N3s=-1/4*(1+r)*(r-s-1)-1/4*(1+r)*(1-s);
       N4r=1/2-1/2*s^2;
       N4s=-(1+r)*s;
       N5r=1/4*(1+s)*(r+s-1)+1/4*(1+r)*(1+s);
       N5s=1/4*(1+r)*(r+s-1)+1/4*(1+r)*(1+s);
       N6r=-r*(1+s);
       N6s=1/2-1/2*r^2;
       N7r=1/4*(1+s)*(r-s+1)+1/4*(r-1)*(1+s);
       N7s=1/4*(r-1)*(r-s+1)-1/4*(r-1)*(1+s);
       N8r=-1/2+1/2*s^2;
       N8s=-(1-r)*s;
        J=[N1r N2r N3r N4r N5r N6r N7r N8r;N1s N2s N3s N4s N5s N6s N7s N8s]*[xa ya;xb yb;xc yc;xd yd;xe ye;xf yf;xg yg;xh yh];
        G1=inv(J);
        G=[G1(1,1:2) 0 0;0 0 G1(2,1:2);G1(2,1:2) G1(1,1:2)];
        P=[N1r N2r N3r N4r N5r N6r N7r N8r 0 0 0 0 0 0 0 0;N1s N2s N3s N4s N5s N6s N7s N8s 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 N1r N2r N3r N4r N5r N6r N7r N8r;0 0 0 0 0 0 0 0 N1s N2s N3s N4s N5s N6s N7s N8s];
        B1=G*P;
        B2a=G1*[N1r N2r N3r N4r N5r N6r N7r N8r zeros(1,16);N1s N2s N3s N4s N5s N6s N7s N8s zeros(1,16)];
        B2b=[zeros(1,8) N1 N2 N3 N4 N5 N6 N7 N8 zeros(1,8);zeros(1,16) N1 N2 N3 N4 N5 N6 N7 N8];
        B2=B2a-B2b;
        B3=B1;
        B=[B1 zeros(3,24);zeros(2,16) B2;zeros(3,24) B3];
        stress=D*B*dispe+Fn;
        Nx=stress(1);Ny=stress(2);Nxy=stress(3);Qx=stress(4);Qy=stress(5);My=stress(6);Mx=stress(7);Mxy=stress(8);
        Nixy=G1*[N1r N2r N3r N4r N5r N6r N7r N8r;N1s N2s N3s N4s N5s N6s N7s N8s];
        Gf=[Nixy zeros(2,32);zeros(2,8) Nixy zeros(2,24);zeros(2,16) Nixy zeros(2,16);zeros(2,24) Nixy zeros(2,8);zeros(2,32) Nixy];
        S=[Nx Nxy zeros(1,6) Mx Mxy;Nxy Ny zeros(1,6) Mxy My;0 0 Nx Nxy 0 0 -Mx -Mxy 0 0;0 0 Nxy Ny 0 0 -Mxy -My 0 0;zeros(1,4) Nx Nxy zeros(1,4);zeros(1,4) Nxy Ny zeros(1,4);0 0 -Mx -Mxy 0 0 Nx*(t^2)/12 Nxy*(t^2)/12 zeros(1,2);0 0 -Mxy -My 0 0 Nxy*(t^2)/12 Ny*(t^2)/12 zeros(1,2);Mx Mxy zeros(1,6) Nx*(t^2)/12 Nxy*(t^2)/12;Mxy My zeros(1,6) Nxy*(t^2)/12 Ny*(t^2)/12];
        kgebr=Gf'*S*Gf*det(J);
        Kger=Kger+kgebr;
             end
        end
        A=[na nb nc nd ne nf ng nh n+na n+nb n+nc n+nd n+ne n+nf n+ng n+nh 2*n+na 2*n+nb 2*n+nc 2*n+nd 2*n+ne 2*n+nf 2*n+ng 2*n+nh 3*n+na 3*n+nb 3*n+nc 3*n+nd 3*n+ne 3*n+nf 3*n+ng 3*n+nh 4*n+na 4*n+nb 4*n+nc 4*n+nd 4*n+ne 4*n+nf 4*n+ng 4*n+nh];
for q=1:40
    for p=1:40
        aq=A(q);
        ap=A(p);
        Kgr(aq,ap)=Kgr(aq,ap)+Kger(q,p);
     end
end
    end
end
K=K+Kgr;
disp=k1\F;
Kgra=zeros(5*n,5*n);
for i=1:2:2*ny-1
    for j=1:2:2*nx-1
        na=T(i,j);
        nb=T(i,j+1);
        nc=T(i,j+2);
        nd=T(i+1,j+2);
        ne=T(i+2,j+2);
        nf=T(i+2,j+1);
        ng=T(i+2,j);
        nh=T(i+1,j);
        dispe=[disp(na);disp(nb);disp(nc);disp(nd);disp(ne);disp(nf);disp(ng);disp(nh);disp(n+na);disp(n+nb);disp(n+nc);disp(n+nd);disp(n+ne);disp(n+nf);disp(n+ng);disp(n+nh);disp(2*n+na);disp(2*n+nb);disp(2*n+nc);disp(2*n+nd);disp(2*n+ne);disp(2*n+nf);disp(2*n+ng);disp(2*n+nh);disp(3*n+na);disp(3*n+nb);disp(3*n+nc);disp(3*n+nd);disp(3*n+ne);disp(3*n+nf);disp(3*n+ng);disp(3*n+nh);disp(4*n+na);disp(4*n+nb);disp(4*n+nc);disp(4*n+nd);disp(4*n+ne);disp(4*n+nf);disp(4*n+ng);disp(4*n+nh)];
        xa=X(1,j);
        xb=X(1,j+1);
        xc=X(1,j+2);
        xd=X(1,j+2);
        xe=X(1,j+2);
        xf=X(1,j+1);
        xg=X(1,j);
        xh=X(1,j);
        ya=Y(i,1);
        yb=Y(i,1);
        yc=Y(i,1);
        yd=Y(i+1,1);
        ye=Y(i+2,1);
        yf=Y(i+2,1);
        yg=Y(i+2,1);
        yh=Y(i+1,1);
        Kgera=zeros(40,40);
        for q=-1/sqrt(3):2/sqrt(3):1/sqrt(3)
            for p=-1/sqrt(3):2/sqrt(3):1/sqrt(3)
                r=q;
                s=p;
       N1=(r-1)*(1-s)*(r+s+1)/4;
       N2=(1-r^2)*(1-s)/2;
       N3=(1+r)*(1-s)*(r-s-1)/4;
       N4=(1+r)*(1-s^2)/2;
       N5=(1+r)*(1+s)*(r+s-1)/4;
       N6=(1-r^2)*(1+s)/2;
       N7=(r-1)*(1+s)*(r-s+1)/4;
       N8=(1-r)*(1-s^2)/2;
       N1r=1/4*(1-s)*(r+s+1)+1/4*(r-1)*(1-s);
       N1s=-1/4*(r-1)*(r+s+1)+1/4*(r-1)*(1-s);
       N2r=-r*(1-s);
       N2s=-1/2+1/2*r^2;
       N3r=1/4*(1-s)*(r-s-1)+1/4*(1+r)*(1-s);
       N3s=-1/4*(1+r)*(r-s-1)-1/4*(1+r)*(1-s);
       N4r=1/2-1/2*s^2;
       N4s=-(1+r)*s;
       N5r=1/4*(1+s)*(r+s-1)+1/4*(1+r)*(1+s);
       N5s=1/4*(1+r)*(r+s-1)+1/4*(1+r)*(1+s);
       N6r=-r*(1+s);
       N6s=1/2-1/2*r^2;
       N7r=1/4*(1+s)*(r-s+1)+1/4*(r-1)*(1+s);
       N7s=1/4*(r-1)*(r-s+1)-1/4*(r-1)*(1+s);
       N8r=-1/2+1/2*s^2;
       N8s=-(1-r)*s;
        J=[N1r N2r N3r N4r N5r N6r N7r N8r;N1s N2s N3s N4s N5s N6s N7s N8s]*[xa ya;xb yb;xc yc;xd yd;xe ye;xf yf;xg yg;xh yh];
        G1=inv(J);
        G=[G1(1,1:2) 0 0;0 0 G1(2,1:2);G1(2,1:2) G1(1,1:2)];
        P=[N1r N2r N3r N4r N5r N6r N7r N8r 0 0 0 0 0 0 0 0;N1s N2s N3s N4s N5s N6s N7s N8s 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 N1r N2r N3r N4r N5r N6r N7r N8r;0 0 0 0 0 0 0 0 N1s N2s N3s N4s N5s N6s N7s N8s];
        B1=G*P;
        B2a=G1*[N1r N2r N3r N4r N5r N6r N7r N8r zeros(1,16);N1s N2s N3s N4s N5s N6s N7s N8s zeros(1,16)];
        B2b=[zeros(1,8) N1 N2 N3 N4 N5 N6 N7 N8 zeros(1,8);zeros(1,16) N1 N2 N3 N4 N5 N6 N7 N8];
        B2=B2a-B2b;
        B3=B1;
        B=[B1 zeros(3,24);zeros(2,16) B2;zeros(3,24) B3];
        stress=D*B*dispe+Fn;
        Nx=stress(1);Ny=stress(2);Nxy=stress(3);Qx=stress(4);Qy=stress(5);My=stress(6);Mx=stress(7);Mxy=stress(8);
        Nixy=G1*[N1r N2r N3r N4r N5r N6r N7r N8r;N1s N2s N3s N4s N5s N6s N7s N8s];
        H1=[Nixy zeros(2,32);zeros(2,8) Nixy zeros(2,24);zeros(2,16) Nixy zeros(2,16);zeros(2,24) Nixy zeros(2,8);zeros(2,32) Nixy];
        P1=[1 zeros(1,9);zeros(1,10);0 0 1 zeros(1,7);zeros(1,10);zeros(1,4) 1 zeros(1,5);zeros(1,10);zeros(1,6) 1*(t^2)/12 zeros(1,3);zeros(1,10);zeros(1,8) 1*(t^2)/12 0;zeros(1,10)];
        kge=H1'*P1*H1*det(J);
        Kgera=Kgera+kge;
             end
        end
        A=[na nb nc nd ne nf ng nh n+na n+nb n+nc n+nd n+ne n+nf n+ng n+nh 2*n+na 2*n+nb 2*n+nc 2*n+nd 2*n+ne 2*n+nf 2*n+ng 2*n+nh 3*n+na 3*n+nb 3*n+nc 3*n+nd 3*n+ne 3*n+nf 3*n+ng 3*n+nh 4*n+na 4*n+nb 4*n+nc 4*n+nd 4*n+ne 4*n+nf 4*n+ng 4*n+nh];
for q=1:40
    for p=1:40
        aq=A(q);
        ap=A(p);
        Kgra(aq,ap)=Kgra(aq,ap)+Kgera(q,p);
     end
end
    end
end
p1=(4*nx)+(4*ny);
T1=T(2:(2*ny+1),(2*nx+1));
T2=T(2:(2*ny),1);
A1=[T(1,1:(2*nx+1)) T1' T((2*ny+1),1:(2*nx)) T2'];
n1=n-p1;
Kgf1=zeros(n1*5+p1-4,5*n);
Kf1=zeros(n1*5+p1-4,5*n);
c1=1;
for i=1:n
    c2=0; 
    for j=1:p1
        if(A1(1,j)==i)
            c2=c2+1;
        end
    end
    if(c2==0)
            Kgf1(c1,:)=Kgra(i,:);
            Kgf1(c1+n1,:)=Kgra(i+n,:);
            Kgf1(c1+2*n1,:)=Kgra(i+2*n,:);
            Kf1(c1,:)=K(i,:);
            Kf1(c1+n1,:)=K(i+n,:);
            Kf1(c1+2*n1,:)=K(i+2*n,:);
            c1=c1+1;
    end
end
c1=1;
for i=1:n
    c2=0;
    for j=1:2*nx+1
        if(i==T(1,j))
            c2=c2+1;
        end
        if(i==T(2*ny+1,j))
            c2=c2+1;
        end
    end
        if(c2==0)
            Kgf1(c1+3*n1,:)=Kgra(i+3*n,:);
            Kf1(c1+3*n1,:)=K(i+3*n,:);
            c1=c1+1;
        end
end
c1=1;
for i=1:n
    c2=0;
    for j=1:2*ny+1
        if(i==T(j,1))
            c2=c2+1;
        end
        if(i==T(j,2*nx+1))
            c2=c2+1;
        end
    end
    if(c2==0)
        Kgf1(c1+4*n1+2*(2*ny-1),:)=Kgra(i+4*n,:);
        Kf1(c1+4*n1+2*(2*ny-1),:)=K(i+4*n,:);
        c1=c1+1;
    end
end
Kgf=zeros(n1*5+p1-4,n1*5+p1-4);
Kf=zeros(n1*5+p1-4,n1*5+p1-4);
c1=1;
for i=1:n
    c2=0;
    for j=1:p1
        if(A1(1,j)==i)
            c2=c2+1;
        end
    end
        if(c2==0)
            Kgf(:,c1)=Kgf1(:,i);
            Kgf(:,c1+n1)=Kgf1(:,i+n);
            Kgf(:,c1+2*n1)=Kgf1(:,i+2*n);
            Kf(:,c1)=Kf1(:,i);
            Kf(:,c1+n1)=Kf1(:,i+n);
            Kf(:,c1+2*n1)=Kf1(:,i+2*n);
            c1=c1+1;
        end
end
c1=1;
for i=1:n
    c2=0;
    for j=1:2*nx+1
        if(i==T(1,j))
            c2=c2+1;
        end
        if(i==T(2*ny+1,j))
            c2=c2+1;
        end
    end
        if(c2==0)
            Kgf(:,c1+3*n1)=Kgf1(:,i+3*n);
            Kf(:,c1+3*n1)=Kf1(:,i+3*n);
            c1=c1+1;
        end
end
c1=1;
for i=1:n
    c2=0;
    for j=1:2*ny+1
        if(i==T(j,1))
            c2=c2+1;
        end
        if(i==T(j,2*nx+1))
            c2=c2+1;
        end
    end
    if(c2==0)
        Kgf(:,c1+4*n1+2*(2*ny-1))=Kgf1(:,i+4*n);
        Kf(:,c1+4*n1+2*(2*ny-1))=Kf1(:,i+4*n);
        c1=c1+1;
    end
end
y=eig(Kf,Kgf);
w2=sort(y);
cr=w2(1)
pcr=cr/1.4007e+005





