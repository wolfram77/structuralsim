function delamparhi
clear
% clc
format compact
tic
fid = fopen('non.txt','w');

nol=8;
alpha1=[0 90 0 90 90 0 90 0 ];
alen=0.127;
bre=0.0127;
th=0.001016;
;row=1600;
% E1 =39.0GPa, E2=E3=8.2GPa, G12=G13=G23=2.9GPa , ?12= ?23= ?31=0.29, Length=150mm, width=25mm, thickness=1.5mm
pr12=0.33;e11=134.4e9;e22=10.34e9;
g12=4.99e9;g13=4.999e9;g23=1.999e9;

nx=8;ny=8;rx=1e20;ry=1e20;rxy=1e20;
kl=2;kr=0;kt=0;kb=0;

%%%%delamination data
tht=th/2.0; nolt=nol/2.0;
thb=th/2.0; nolb=nol/2.0;
alphat=[0 90 0 90];alphab=[90 0 90 0];

ndel=4;  %%%% 25% central delamination
% de =0;
 de(1:ndel)=[28 29 30 31];
%  de(1:ndel)=[26 27 28 29 30 31 32 33];
%de(1:ndel)=[8 9 10 24 25 26 39 40 41 56 57 58];
%de(1:ndel)=[9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 ...
 %36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(e11==e22)
g12=e11/(2.0*(1+pr12));
g13=g12;g23=g12;
end

pr21=e22*pr12/e11;
nel=nx*ny;
[ax,by,nnd,ndarr,mat,nbignod,nbig,ncon]=gener(alen,bre, ...
                nx,ny,kl,kr,kt,kb);

[C,zk,Qbar,A] = compo(e11,e22,g12,g13,g23,pr12,th,alpha1,row,nol);
CT = top(e11,e22,g12,g13,g23,pr12,tht,alphat,row,nolt);
CB = bottom(e11,e22,g12,g13,g23,pr12,thb,alphab,row,nolb);
DC = delamcompo(CT,CB);

SK=zeros(nbig,nbig);
%SKG=zeros(nbig,nbig);

for ln=1:nel
   
  if (any(de==ln))
   SK=stiff(ax,by,nel,nnd,nbig,DC,rx,ry,rxy,ln,SK);
   %SKG=gestiff(ax,by,nel,nnd,nbig,C,rx,ry,rxy,ln,SKG,th);
 else
   
    SK=stiff(ax,by,nel,nnd,nbig,C,rx,ry,rxy,ln,SK);
    %SKG=gestiff(ax,by,nel,nnd,nbig,C,rx,ry,rxy,ln,SKG,th);
  end
end
SKG=zeros(nbig,nbig);
 for ln=1:nel 
  SKG=gestiff(ax,by,nel,nnd,nbig,C,rx,ry,rxy,ln,SKG,th);
end

%F=zeros(925,1);
%disp=SK\F;
%Fn=zeros(8,1);
%for ln=1:nel
%stress=C*SK*disp+Fn
%end
SM = massfsdt(row,zk,nol,nel,ax,by,nbig,nnd);
 ev=eig(SK,SM);
 w=sqrt(min(ev));
 nfrq=w/2/pi
 ndfrq=alen^2/th*sqrt(row/e22);
lamda=w*ndfrq

evg=eig(SK,SKG);
wg=sqrt(min(evg));
Kf1=SK;
Kf2=SK;
Kgf1=SKG;
Mf1=SM;
y=eig(SK,SKG);
w2=sort(y);
cr=w2(1)
cr2=w2(1)* bre
pcr=cr/6.9736e+004;
pcr2=cr*alen*alen/(e22*th*th*th)
pcr1=6.9736e+004;
factor1=0.2;
factor2=0.3;
Kf3=Kf2-(factor1*cr*Kgf1)+(0.5*factor2*cr*Kgf1);
%Kf3=Kf2-(factor1*cr*Kgf1)-(0.5*factor2*cr*Kgf1);
y2=eig(Kf3,Mf1);
 w4=sort(y2);
 lamda1=sqrt(w4(1:5))*(alen^2/th)*sqrt(row/e22);
 lamda2=2*lamda1

 %ndfrq=wg/2/pi

toc

  
 function SM = massfsdt(row,zk,nol,nel,ax,by,nbig,nnd)
i=1:nbig;
j=1:nbig;
SM(i,j)=0.0;
% function for calculating mass matrix
p1=0.0;p2=0.0;p3=0.0;

for ln=1:nol
    for i=1:7
   H(i)=(zk(ln)^i-zk(ln+1)^i)/i;
   end
    p1 = p1+row*H(1);
	p2 = p2+row*H(2);
	p3 = p3+row*H(3);
end   


    MC(1,1)=p1;	MC(1,4)=p2;	MC(2,2)=p1;	MC(2,5)=p2;
	MC(3,3)=p1;	MC(4,1)=p2;	MC(4,4)=p3;	MC(5,2)=p2;
	MC(5,5)=p3;

 for ln=1:nel
    for j=1:8
    	xl(j)=ax(ln,j);
    	yl(j)=by(ln,j);
    end

    for I=1:40
        ND(I)=nnd(ln,I);
    end
  
%Initialising element mass matrix
i=1:40;
j=1:40;
em(i,j)=0.0;
for ix=1:2
    for iy=1:2
[sf,sfdg,da] = shaped(xl,yl,ix,iy) ;
 for i =1:8
    k1 = 5*(i-1)+1;
    k2 = k1+1;
    k3 = k1+2;
    k4 = k1+3;
    k5 = k1+4;
    bm(1,k1) = sf(i);
	bm(2,k2) = sf(i);
	bm(3,k3) = sf(i);
	bm(4,k4) = sf(i);
	bm(5,k5) = sf(i);
 end
 % Calculation of Mass matrix
   em=em+bm'*MC*bm*da;
 end %End of ix loop
end  %End of ix loop

  SM = assmbl(em,ND,40,SM);
end %End of ln loop
    
    
    function SK=stiff(ax,by,nel,nnd,nbig,C,rx,ry,rxy,ln,SK)

% for ln=1:nel

    for j=1:8
    	xl(j)=ax(ln,j);
    	yl(j)=by(ln,j);
    end

    for I=1:40
        ND(I)=nnd(ln,I);
    end
    
%Initialising element stiffness matrix
i=1:40;
j=1:40;
eck(i,j)=0.0;
%gck(i,j)=0.0;


for ix=1:2
    for iy=1:2
[sf,sfdg,da] = shaped(xl,yl,ix,iy) ;
        for i =1:8
 k1 = 5*(i-1)+1; k2 = k1+1; k3 = k1+2; k4 = k1+3; k5 = k1+4;
      b(1,k1) = sfdg(1,i);  b(1,k3) = sf(i)/rx;
	  b(2,k2) = sfdg(2,i);  b(2,k3) = sf(i)/ry;
      b(3,k1) = sfdg(2,i);  b(3,k2) = sfdg(1,i);
	  %b(3,k3) = 2.0*sf(i)/rxy;
      b(4,k4) = sfdg(1,i);  b(5,k5) = sfdg(2,i);
      b(6,k4) = sfdg(2,i); b(6,k5) = sfdg(1,i);
       b(7,k3) = sfdg(1,i);
      b(7,k4) = sf(i); 
      b(8,k3) = sfdg(2,i); b(8,k5) = sf(i);
        end
        eck=eck+b'*C*b*da;
        
end %End of ix loop
end  %End of ix loop

SK = assmbl(eck,ND,40,SK);
% end %End of ln loop

function SKG=gestiff(ax,by,nel,nnd,nbig,C,rx,ry,rxy,ln,SKG,th)

% for ln=1:nel
    for j=1:8
    	xl(j)=ax(ln,j);
    	yl(j)=by(ln,j);
    end

    for I=1:40
        ND(I)=nnd(ln,I);
    end
    i=1:40;
j=1:40;
%eck(i,j)=0.0;
gck(i,j)=0.0;


for ix=1:2
    for iy=1:2
[sf,sfdg,da] = shaped(xl,yl,ix,iy) ;
    
    %gck(i,j)=0.0;
    for i =1:8
 k1 = 5*(i-1)+1; k2 = k1+1; k3 = k1+2; k4 = k1+3; k5 = k1+4;
      bg(1,k1) = sfdg(1,i); 
	  bg(2,k1) = sfdg(2,i); 
      bg(3,k2) = sfdg(1,i);
	  bg(4,k2) = sfdg(2,i);
     bg(5,k1)=  -sf(i)/rx;  bg(5,k3) = sfdg(1,i);
      bg(6,k2)= -sf(i)/ry; bg(6,k3) = sfdg(2,i);
      bg(7,k4) = sfdg(1,i);
      bg(8,k4) = sfdg(2,i);
      bg(9,k5) = sfdg(1,i);
      bg(10,k5) = sfdg(2,i);
        end   
%s=zeros(10,10);
%th=.005
 s=[1 0 zeros(1,6) 0 0;
     0 0 zeros(1,6) 0 0;
     0 0 1 0 0 0 0 0 0 0;
     0 0 0 0 0 0 0 0 0 0;
     zeros(1,4) 1 0 zeros(1,4);
     zeros(1,4) 0 0 zeros(1,4)
      0 0 0 0 0 0 1*(th^2)/12 0*(th^2)/12 zeros(1,2);
      0 0 0 0 0 0 0*(th^2)/12 0*(th^2)/12 zeros(1,2);
      0 0 zeros(1,6) 1*(th^2)/12 0*(th^2)/12;
     0 0 zeros(1,6) 0*(th^2)/12 0*(th^2)/12];
gck=gck+bg'*s*bg*da;
    end
end
SKG = assmbl(gck,ND,40,SKG);

    function SYSM = assmbl(ee,ND,ndofe,SYSM)
for i=1:ndofe
	for j=1:ndofe
      k1=ND(i);
	  k2=ND(j);
    if k1~=0 && k2~=0 
    	SYSM(k1,k2)=SYSM(k1,k2)+ee(i,j);
    end %if
   end %i
  end %j

function [C,zk,Qbar,A] = compo(e11,e22,g12,g13,g23,pr12,th,alpha1,row,nol)
% Subfunction for calculating constititive matrix
% calculating thickness of each layer (Equal)
	thl=th/nol;
	zk(1)=th/2.0;
	 for i=2:nol+1
    	zk(i)=zk(i-1)-thl;
     end
    pr21=e22*pr12/e11;
	c(1,1) = e11/(1.0-pr12*pr21);
    c(1,2) = c(1,1)*pr21 ;
	c(2,2) = e22/(1.0-pr12*pr21);
	c(2,1) = c(2,2)*pr12;
	c(3,3) = g12;
    c(4,4) = g13;
    c(5,5) = g23;	

i=1:2;
j=1:2;
S(i,j)=0.0;

i=1:3;
j=1:3;
A(i,j)=0.0;
B(i,j)=0.0;
D(i,j)=0.0;
for ln=1:nol
   	alpha=alpha1(ln)*pi/180.0;
	T(1,1)=cos(alpha)^2;
	T(1,2)=sin(alpha)^2;
	T(1,3)=sin(alpha)*cos(alpha);
	T(2,1)=sin(alpha)^2;
	T(2,2)=T(1,1);
	T(2,3)=-T(1,3);
	T(3,1)=-2*sin(alpha)*cos(alpha);
	T(3,2)=-T(3,1);
	T(3,3)=cos(alpha)^2-sin(alpha)^2;
    T(4,4)=cos(alpha);
    T(4,5)=-sin(alpha);
    T(5,4)=sin(alpha);    
    T(5,5)=cos(alpha);
    QT=c*T;
    Qbar=T'*QT;

    for i=1:3
        for j=1:3
            A(i,j)=A(i,j)+Qbar(i,j)*(zk(ln)-zk(ln+1));	
        end
        end

    for i=1:3
        for j=1:3
            B(i,j)=B(i,j)+Qbar(i,j)*(zk(ln)^2-zk(ln+1)^2);	
        end
    end

    for i=1:3
        for j=1:3
            D(i,j)=D(i,j)+Qbar(i,j)*(zk(ln)^3-zk(ln+1)^3);	
        end
    end
   
S(1,1)=S(1,1)+Qbar(4,4)*(zk(ln)-zk(ln+1));
S(1,2)=S(1,2)+Qbar(4,5)*(zk(ln)-zk(ln+1));
S(2,1)=S(2,1)+Qbar(5,4)*(zk(ln)-zk(ln+1));
S(2,2)=S(2,2)+Qbar(5,5)*(zk(ln)-zk(ln+1));
end 

ee=zeros(3,2);ee1=zeros(2,3);
C=[A 0.5*B ee;0.5*B D/3.0 ee;ee1 ee1 S*5.0/6.0];

 function CT = top(e11,e22,g12,g13,g23,pr12,tht,alphat,row,nolt);
    thlt=tht/nolt;
	zk(1)=tht;
    
    for i=2:nolt+1
    	zk(i)=zk(i-1)-thlt;
    end
     
    z0t=tht/2;

    pr21=e22*pr12/e11;
	c(1,1) = e11/(1.0-pr12*pr21);
    c(1,2) = c(1,1)*pr21 ;
	c(2,2) = e22/(1.0-pr12*pr21);
	c(2,1) = c(2,2)*pr12;
	c(3,3) = g12;
    c(4,4) = g13;
    c(5,5) = g23;	

i=1:2;
j=1:2;
S(i,j)=0.0;

i=1:3;
j=1:3;
A(i,j)=0.0;
B(i,j)=0.0;
D(i,j)=0.0;
for ln=1:nolt
   	alpha=alphat(ln)*pi/180.0;
	T(1,1)=cos(alpha)^2;
	T(1,2)=sin(alpha)^2;
	T(1,3)=sin(alpha)*cos(alpha);
	T(2,1)=sin(alpha)^2;
	T(2,2)=T(1,1);
	T(2,3)=-T(1,3);
	T(3,1)=-2*sin(alpha)*cos(alpha);
	T(3,2)=-T(3,1);
	T(3,3)=cos(alpha)^2-sin(alpha)^2;
    T(4,4)=cos(alpha);
    T(4,5)=-sin(alpha);
    T(5,4)=sin(alpha);    
    T(5,5)=cos(alpha);
    QT=c*T;
    Qbar=T'*QT;

for i=1:3
        for j=1:3
            A(i,j)=A(i,j)+Qbar(i,j)*(zk(ln)-zk(ln+1));	
        end
        end

    for i=1:3
        for j=1:3
            B(i,j)=B(i,j)+Qbar(i,j)*(zk(ln)^2-zk(ln+1)^2)/2.0;	
        end
    end

    for i=1:3
        for j=1:3
            D(i,j)=D(i,j)+Qbar(i,j)*(zk(ln)^3-zk(ln+1)^3)/3.0;
        end
    end
   
S(1,1)=S(1,1)+Qbar(4,4)*(zk(ln)-zk(ln+1));
S(1,2)=S(1,2)+Qbar(4,5)*(zk(ln)-zk(ln+1));
S(2,1)=S(2,1)+Qbar(5,4)*(zk(ln)-zk(ln+1));
S(2,2)=S(2,2)+Qbar(5,5)*(zk(ln)-zk(ln+1));
end 

ee=zeros(3,2);ee1=zeros(2,3);
CTP=[A B ee;B D ee;ee1 ee1 S*5.0/6.0];

CT(1,1)=CTP(1,1); CT(1,2)=CTP(1,2); CT(1,3)=CTP(1,3);
CT(2,1)=CTP(2,1);CT(2,2)=CTP(2,2);CT(2,3)=CTP(2,3);
CT(3,1)=CTP(3,1);CT(3,2)=CTP(3,2);CT(3,3)=CTP(3,3);
CT(1,4)=CTP(1,4);CT(1,5)=CTP(1,5);CT(1,6)=CTP(1,6);
CT(2,4)=CTP(2,4);CT(2,5)=CTP(2,5);CT(2,6)=CTP(2,6);
CT(3,4)=CTP(3,4);CT(3,5)=CTP(3,5);CT(3,6)=CTP(3,6);
CT(4,1)=CTP(1,4)-z0t*CTP(1,1);CT(4,2)=CTP(1,5)-z0t*CTP(1,2);CT(4,3)=CTP(1,6)-z0t*CTP(1,3);
CT(5,1)=CTP(2,4)-z0t*CTP(2,1);CT(5,2)=CTP(2,5)-z0t*CTP(2,2);CT(5,3)=CTP(2,6)-z0t*CTP(2,3);
CT(6,1)=CTP(3,4)-z0t*CTP(3,1);CT(6,2)=CTP(3,5)-z0t*CTP(3,2);CT(6,3)=CTP(3,6)-z0t*CTP(3,3);
CT(4,4)=CTP(4,4)+z0t^2*CTP(1,1)-2.0*z0t*CTP(1,4)+z0t*CTP(1,4)-z0t^2*CTP(1,1);
CT(4,5)=CTP(4,5)+z0t^2*CTP(1,2)-2.0*z0t*CTP(1,5)+z0t*CTP(1,5)-z0t^2*CTP(1,2);
CT(4,6)=CTP(4,6)+z0t^2*CTP(1,3)-2.0*z0t*CTP(1,6)+z0t*CTP(1,6)-z0t^2*CTP(1,3);
CT(5,4)=CTP(5,4)+z0t^2*CTP(2,1)-2.0*z0t*CTP(2,4)+z0t*CTP(2,4)-z0t^2*CTP(2,1);
CT(5,5)=CTP(5,5)+z0t^2*CTP(2,2)-2.0*z0t*CTP(2,5)+z0t*CTP(2,5)-z0t^2*CTP(2,2);
CT(5,6)=CTP(5,6)+z0t^2*CTP(2,3)-2.0*z0t*CTP(2,6)+z0t*CTP(2,6)-z0t^2*CTP(2,3);
CT(6,4)=CTP(6,4)+z0t^2*CTP(3,1)-2.0*z0t*CTP(3,4)+z0t*CTP(3,4)-z0t^2*CTP(3,1);
CT(6,5)=CTP(6,5)+z0t^2*CTP(3,2)-2.0*z0t*CTP(3,5)+z0t*CTP(3,5)-z0t^2*CTP(3,2);
CT(6,6)=CTP(6,6)+z0t^2*CTP(3,3)-2.0*z0t*CTP(3,6)+z0t*CTP(3,6)-z0t^2*CTP(3,3);

for i =7:8
    for j =1:6
        CT(i,j)=CTP(i,j);
    end
end
for i =1:6
    for j =7:8
        CT(i,j)=CTP(i,j);
    end
end
CT(7,7)=S(1,1);CT(7,8)=S(1,2);CT(8,7)=S(2,1);CT(8,8)=S(2,2);


 function CB = bottom(e11,e22,g12,g13,g23,pr12,thb,alphab,row,nolb);
    thlb=thb/nolb;
	zk(1)=0.0;
    
    for i=2:nolb+1
    	zk(i)=zk(i-1)-thlb;
    end
        z0b=-thb/2;
    
    pr21=e22*pr12/e11;
	c(1,1) = e11/(1.0-pr12*pr21);
    c(1,2) = c(1,1)*pr21 ;
	c(2,2) = e22/(1.0-pr12*pr21);
	c(2,1) = c(2,2)*pr12;
	c(3,3) = g12;
    c(4,4) = g13;
    c(5,5) = g23;	

i=1:2;
j=1:2;
S(i,j)=0.0;

i=1:3;
j=1:3;
A(i,j)=0.0;
B(i,j)=0.0;
D(i,j)=0.0;
for ln=1:nolb
   	alpha=alphab(ln)*pi/180.0;
	T(1,1)=cos(alpha)^2;
	T(1,2)=sin(alpha)^2;
	T(1,3)=sin(alpha)*cos(alpha);
	T(2,1)=sin(alpha)^2;
	T(2,2)=T(1,1);
	T(2,3)=-T(1,3);
	T(3,1)=-2*sin(alpha)*cos(alpha);
	T(3,2)=-T(3,1);
	T(3,3)=cos(alpha)^2-sin(alpha)^2;
    T(4,4)=cos(alpha);
    T(4,5)=-sin(alpha);
    T(5,4)=sin(alpha);    
    T(5,5)=cos(alpha);
    QT=c*T;
    Qbar=T'*QT;

for i=1:3
        for j=1:3
            A(i,j)=A(i,j)+Qbar(i,j)*(zk(ln)-zk(ln+1));	
        end
        end

    for i=1:3
        for j=1:3
            B(i,j)=B(i,j)+Qbar(i,j)*(zk(ln)^2-zk(ln+1)^2)/2.0;	
        end
    end

    for i=1:3
        for j=1:3
            D(i,j)=D(i,j)+Qbar(i,j)*(zk(ln)^3-zk(ln+1)^3)/3.0;
        end
    end
   
S(1,1)=S(1,1)+Qbar(4,4)*(zk(ln)-zk(ln+1));
S(1,2)=S(1,2)+Qbar(4,5)*(zk(ln)-zk(ln+1));
S(2,1)=S(2,1)+Qbar(5,4)*(zk(ln)-zk(ln+1));
S(2,2)=S(2,2)+Qbar(5,5)*(zk(ln)-zk(ln+1));
end 

ee=zeros(3,2);ee1=zeros(2,3);
CBP=[A B ee;B D ee;ee1 ee1 S*5.0/6.0];   
 
CB(1,1)=CBP(1,1);CB(1,2)=CBP(1,2);CB(1,3)=CBP(1,3);
CB(2,1)=CBP(2,1);CB(2,2)=CBP(2,2);CB(2,3)=CBP(2,3);
CB(3,1)=CBP(3,1);CB(3,2)=CBP(3,2);CB(3,3)=CBP(3,3);
CB(1,4)=CBP(1,4);CB(1,5)=CBP(1,5);CB(1,6)=CBP(1,6);
CB(2,4)=CBP(2,4);CB(2,5)=CBP(2,5);CB(2,6)=CBP(2,6);
CB(3,4)=CBP(3,4);CB(3,5)=CBP(3,5);CB(3,6)=CBP(3,6);
CB(4,1)=CBP(1,4)-z0b*CBP(1,1);CB(4,2)=CBP(1,5)-z0b*CBP(1,2);CB(4,3)=CBP(1,6)-z0b*CBP(1,3);
CB(5,1)=CBP(2,4)-z0b*CBP(2,1);CB(5,2)=CBP(2,5)-z0b*CBP(2,2);CB(5,3)=CBP(2,6)-z0b*CBP(2,3);
CB(6,1)=CBP(3,4)-z0b*CBP(3,1);CB(6,2)=CBP(3,5)-z0b*CBP(3,2);CB(6,3)=CBP(3,6)-z0b*CBP(3,3);
CB(4,4)=CBP(4,4)+z0b^2*CBP(1,1)-2.0*z0b*CBP(1,4)+z0b*CBP(1,4)-z0b^2*CBP(1,1);
CB(4,5)=CBP(4,5)+z0b^2*CBP(1,2)-2.0*z0b*CBP(1,5)+z0b*CBP(1,5)-z0b^2*CBP(1,2);
CB(4,6)=CBP(4,6)+z0b^2*CBP(1,3)-2.0*z0b*CBP(1,6)+z0b*CBP(1,6)-z0b^2*CBP(1,3);
CB(5,4)=CBP(5,4)+z0b^2*CBP(2,1)-2.0*z0b*CBP(2,4)+z0b*CBP(2,4)-z0b^2*CBP(2,1);
CB(5,5)=CBP(5,5)+z0b^2*CBP(2,2)-2.0*z0b*CBP(2,5)+z0b*CBP(2,5)-z0b^2*CBP(2,2);
CB(5,6)=CBP(5,6)+z0b^2*CBP(2,3)-2.0*z0b*CBP(2,6)+z0b*CBP(2,6)-z0b^2*CBP(2,3);
CB(6,4)=CBP(6,4)+z0b^2*CBP(3,1)-2.0*z0b*CBP(3,4)+z0b*CBP(3,4)-z0b^2*CBP(3,1);
CB(6,5)=CBP(6,5)+z0b^2*CBP(3,2)-2.0*z0b*CBP(3,5)+z0b*CBP(3,5)-z0b^2*CBP(3,2);
CB(6,6)=CBP(6,6)+z0b^2*CBP(3,3)-2.0*z0b*CBP(3,6)+z0b*CBP(3,6)-z0b^2*CBP(3,3);

for i =7:8
    for j =1:6
        CB(i,j)=CBP(i,j);
    end
end
for i =1:6
    for j =7:8
        CB(i,j)=CBP(i,j);
    end
end
CB(7,7)=S(1,1);CB(7,8)=S(1,2);CB(8,7)=S(2,1);CB(8,8)=S(2,2);

function DC = delamcompo(CT,CB)
% Subfunction for calculating delamination constititive matrix
% calculating thickness of each layer (Equal)
	   
  for i =1:8
      for j =1:8
          DC(i,j)=CT(i,j)+CB(i,j);
      end
  end



function [ax,by,nnd,ndarr,mat,nbignod,nbig,ncon]=gener(alen,bre, ...
                       nx,ny,kl,kr,kt,kb)
aaa=alen/ny;
bbb=bre/nx;
nel=nx*ny;
lnum=1;
for ll=1:nx
    for i=1:ny
        ax(lnum,1)= aaa*(i-1); 
        ax(lnum,2)=aaa*i;
    	ax(lnum,3)=aaa*i;
    	ax(lnum,4)=aaa*(i-1);
    	ax(lnum,5)=(ax(lnum,1)+ax(lnum,2))/2.0;
    	ax(lnum,6)=aaa*i;
    	ax(lnum,7)=ax(lnum,5);
    	ax(lnum,8)=ax(lnum,1);
    	 
    	by(lnum,1)=bbb*(ll-1);
    	by(lnum,2)=bbb*(ll-1);
    	by(lnum,3)=bbb*ll;
    	by(lnum,4)=bbb*ll;
    	by(lnum,5)=by(lnum,1);
    	by(lnum,6)=(by(lnum,1)+by(lnum,4))/2.0;
    	by(lnum,7)=by(lnum,3);
    	by(lnum,8)=by(lnum,6);
      lnum=lnum+1;
     end
 end
 kount=0;
  for  i=1:2:nx*2+1
     for j=1:ny*2+1  
       kount=kount+1;
       mat(i,j)=kount;
     end   
 
   if i ~= nx*2+1 
     for j=1:2:ny*2+1  
      kount=kount+1;
      mat(i+1,j)=kount;
      end
    end
end
       lnum=1;
     for i=1:2:2*nx-1
       for  j=1:2:2*ny-1
       ncon(lnum,1)=mat(i,j); 
       ncon(lnum,2)=mat(i,j+2); 
       ncon(lnum,3)=mat(i+2,j+2); 
       ncon(lnum,4)=mat(i+2,j);
       ncon(lnum,5)=mat(i,j+1) ;
       ncon(lnum,6)=mat(i+1,j+2);
       ncon(lnum,7)=mat(i+2,j+1);
       ncon(lnum,8)=mat(i+1,j) ;
       lnum=lnum+1;
     end
 end
  nbignod=mat(nx*2+1,ny*2+1);
  i=1:nbignod;  
  j=1:5;
  ndarr(i,j)=1;

   
if kl==1
  for i=1:nx*2+1
    ndarr(mat(i,1),2)=0;
    ndarr(mat(i,1),3)=0;
    ndarr(mat(i,1),5)=0;
  end
end    
    if kl==2
     i=1:nx*2+1;
     j=1:5;
     ndarr(mat(i,1),j)=0;
    end    

if kr==1
  for i=1:nx*2+1
    ndarr(mat(i,ny*2+1),2)=0;
    ndarr(mat(i,ny*2+1),3)=0;
    ndarr(mat(i,ny*2+1),5)=0;
  end
end    
    if kr==2
        i=1:nx*2+1;
        j=1:5;
        ndarr(mat(i,ny*2+1),j)=0;
    end    

if kt==1
  for i=1:ny*2+1
    ndarr(mat(nx*2+1,i),1)=0;
    ndarr(mat(nx*2+1,i),3)=0;
    ndarr(mat(nx*2+1,i),4)=0;
  end
end    
       if kt==2
        i=1:ny*2+1;
        j=1:5;
        ndarr(mat(nx*2+1,i),j)=0;
       end    

if kb==1
  for i=1:ny*2+1
    ndarr(mat(1,i),1)=0;
    ndarr(mat(1,i),3)=0;
    ndarr(mat(1,i),4)=0;
  end
end    
if kb==2
    i=1:ny*2+1;
    j=1:5;
    ndarr(mat(1,i),j)=0;
end    

kount=0;
for i=1:nbignod
    for j=1:5
        if ndarr(i,j)~=0
            kount=kount+1;
            ndarr(i,j)=kount;
        end 
    end 
end
nbig=kount;

for i=1:nel
    for j=1:8
        for k=1:5
            lll=(j-1)*5+k;
            nnd(i,lll)=ndarr(ncon(i,j),k);
        end
    end 
end    

function [sf,sfdg,da] = shaped(xl,yl,ix,iy) 
% This subfunction gives the shapefunctions 
%  and their derivatives
r=[-1.0,1.0,1.0,-1.0,0.0,1.0,0.0,-1.0];
s=[-1.0,-1.0,1.0,1.0,-1.0,0.0,1.0,0.0];
gp=[-0.5773502691896,0.5773502691896];
wg=[1 1];
    for i =1:8
	aa = (1.0 + r(i)*gp(ix));
	bb = (1.0 + s(i)*gp(iy));
    	if i<=4
        	sf(i) = 0.25*aa*bb*(aa+bb-3.0);
        	sfd(1,i) = 0.25*(2.0*aa+bb-3.0)*bb*r(i);
        	sfd(2,i) = 0.25*(2.0*bb+aa-3.0)*aa*s(i);
    	else
        	aa=aa+(r(i)^2-1.0)*gp(ix)^2;
        	bb=bb+(s(i)^2-1.0)*gp(iy)^2;
        	sf(i) = 0.5*aa*bb;
        	sfd(1,i)=0.5*(r(i)+2.0*(r(i)^2-1.0)*gp(ix))*bb;
        	sfd(2,i)=0.5*(s(i)+2.0*(s(i)^2-1.0)*gp(iy))*aa;
        end 
    end 

coor=[xl; yl];

xjac=sfd*coor';
djac=det(xjac);
da=djac*wg(ix)*wg(iy);
sfdg=inv(xjac)*sfd;

