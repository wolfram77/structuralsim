clear all
clc
E=69e3;
b=25;
d=25;
% E=10e06;
% b=1;
% d=1;
A=b*d;
I=b*d^3/12;
w=4.5;
% w=10;
%load vector for a udl=[w*L/2; w*L^2/12; w*L/2; -w*L^2/12]
L=6000;
% L=20;
theta=[90*pi/180;0];
K=zeros(9);
dof=[1 2 3 4 5 6;4 5 6 7 8 9];
n=size(dof,1);
for i=1:n
    k=[E*A/L 0           0         -E*A/L  0           0
       0     12*E*I/L^3  6*E*I/L^2  0     -12*E*I/L^3  6*E*I/L^2;
       0     6*E*I/L^2   4*E*I/L    0     -6*E*I/L^2   2*E*I/L;
      -E*A/L 0           0          E*A/L  0           0
       0    -12*E*I/L^3 -6*E*I/L^2  0      12*E*I/L^3 -6*E*I/L^2;
       0     6*E*I/L^2   2*E*I/L    0     -6*E*I/L^2   4*E*I/L];
    C=cos(theta(i));
    S=sin(theta(i));
    T=eye(6);
    t=[C S;-S C];
    T(1:2,1:2)=t;T(4:5,4:5)=t;
    ki=T'*k*T;
    gdof=dof(i,:);
    kelm(:,:,i)=ki;
    K(gdof,gdof)=K(gdof,gdof)+ki;
end
K
P=[0; -w*L/2; -w*L^2/12; 0; -w*L/2; w*L^2/12]
kr =[1 2 3 7 8 9];
kt =1:9;
kdof=setdiff(kt,kr);
ke=K(kdof,kdof);
f=P(1:3);
u=ke\f;
U=[0;0;0;u(1);u(2);u(3);0;0;0]
F=K*U
for i=1:n
     disp =U(dof(i,:));
     if(i==1)
     sigma(:,:,i)=kelm(:,:,i)*disp;
     end
     if(i==2)
     sigma(:,:,i)=kelm(:,:,i)*disp-P;
     end
end
sigma


