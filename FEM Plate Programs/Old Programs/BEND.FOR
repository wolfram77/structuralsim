c       Only load vector is to be modified to evaluate the deflection
c       In this program, the load vector contains a central load 
c       of p(at center)=1.0
c       Deflections are non-dimensionalised




c*************************  reading all datas ***********************
	implicit double precision(a-h,o-z)
	integer cht(2000),nds(2000),nd(24)
	DIMENSION EKK(24,24),XL(8),YL(8) 
	DIMENSION XCO(4,8),YCO(4,8),NND(150,24)
	DIMENSION a(150000),p(2000) 
	DIMENSION CA(150000)
	open(unit=10,file='bend.in')
	open(unit=20,file='bend.out')
	kount=0
	read(10,*)alen,bre,th,e,pr,nx,ny,kp
	call gener(alen,bre,nx,ny,nel,kp,nper,nbig,xco,yco,
     1  nnd,nzx,nzy)
	do 850 j=1,8
	xl(j)=xco(1,j)
	yl(j)=yco(1,j)
850     continue
	call bstif(EKK,XL,YL,TH,PR,E)

c************************      LOOP6     ******************************
	NSKY=0
	DO 11 I=1,nbig
	p(i)=0.0
	CHT(I)=0
11      NDS(I)=0
	MBAND=0
	DO 1101 LNUM=1,NEL
	DO 1301 I=1,24
1301    ND(I)=NND(LNUM,I)
	CALL COLUMH(CHT,ND,24,NBIG)
1101    CONTINUE
	p(nnd( (nx* (ny/2-1) +nx/2),7))=1.0
	DO 1102 LNUM=1,NEL
	NDO1=NBIG+1
	CALL CADNUM(CHT,NDS,NBIG,NDO1,NSKY,MBAND)
1102    CONTINUE
	write(*,*)nsky
	do 970 i=1,nsky
	a(i)=0.0
970     continue
	do 10897 i=1,24
10897   write(20,10898)(ekk(i,j),j=1,6)
10898   format(6(f12.6,1x))
	do 1200 lnum=1,nel
	DO 5301 I=1,24
5301    ND(I)=NND(LNUM,I)
	do 419 i=1,8
	xl(i)=xco(1,i)
419     yl(i)=yco(1,i)
	CALL PASSEM (A,EKK,NDS,ND,24,NBIG,NSKY,24)
1200    continue
9600    format(2x,i3,2x,2(e15.7,2x))
	nwk=nsky
	nn=nbig
	call s21(ca,a,nwk)
	CALL PASOLV(A,P,NDS,NBIG,NEQ1,NSKY,inde )
	xxx=p(nnd( (nx* (ny/2-1) +nx/2),7))
	xxx=xxx*e*th/(12.0*(1-PR*PR))/ALEN/ALEN
	write(*,*)xxx

	stop
	end  





	    subroutine bstif(ek,xl,yl,th,pr,e)
       IMPLICIT double precision(A-H,O-Z)
	    dimension ek(24,24),c(5,5),xl(8),yl(8),r(8),s(8)
	  dimension xjaci(2,2),sfdg(2,8),sf(8),b(5,24),xjac(2,2)
	   dimension db(5,24),gp(2),wg(2),sfd(2,8)
	     data wg/1.D0,1.D0/
	     data gp/-0.5773502691896,0.5773502691896/
	     data r/-1.D0,1.D0,1.D0,-1.D0,0.D0,1.D0,0.D0,-1.D0/
	     data s/-1.D0,-1.D0,1.D0,1.D0,-1.D0,0.D0,1.D0,0.D0/
	     do 1657 i=1,5
	     do 1657 j=1,5
1657         c(i,j)=0.0
	    c(1,1) = e*th**3/(12.0*(1.0-pr**2))
	     c(1,2) = c(1,1)*pr
	     c(2,1) = c(1,2)
	     c(2,2) = c(1,1)
	     c(3,3) = c(1,1)*(1.0-pr)/2.0
c            c(4,4)=70000.0   > 5000.0
c            c(4,4) = e*th**3/(12.0*(1.0-pr**2))*35000.0/bre**2
c            c(4,4) = e*th/(2.0*(1.0+pr))  ~ 1800.0
	     c(4,4) = e*th/(2.0*(1.0+pr))
	     c(5,5) = c(4,4)
C            INTIALIZE STIFFNESS AND STABILITY MATRIX
	     do 20 i = 1,24
	     do 20 j = 1,24
20           ek(i,j) = 0.0
C            COMPUTE THE SS-MATRIX
C         ENTER THE LOOP FOR INTEGRATION
		  do 170 ix =1,2
		  do 170 iy =1,2
C         CALUCALATE SHAPE FUNCTIONS AND THEIR DERVATIVES
		  do 50 i =1,8
		  aa = (1.0 + r(i)*gp(ix))
		  bb = (1.0 + s(i)*gp(iy))
		  if (i .gt. 4)  goto 40
		  sf(i) = 0.25*aa*bb*(aa+bb-3.0)
		  sfd(1,i) = 0.25*bb*(2.0*aa+bb-3.0)*r(i)
		  sfd(2,i) = 0.25*aa*(2.0*bb+aa-3.0)*s(i)
		  goto 50
40        aa = aa + (r(i)**2-1.0)*gp(ix)**2
		  bb = bb + (s(i)**2-1.0)*gp(iy)**2
		  sf(i) = 0.5*aa*bb
		  sfd(1,i) = 0.5*(r(i)+2.0*(r(i)**2-1.0)*gp(ix))*bb
		  sfd(2,i) = 0.5*(s(i)+2.0*(s(i)**2-1.0)*gp(iy))*aa
50               continue
		  do 60 i =1,2
		  do 60 j =1,2
60        xjac(i,j) = 0.0
		  do 70 i =1,8
		  do 70 k =1,2
		  xjac(k,1) = xjac(k,1)+xl(i)*sfd(k,i)
70        xjac(k,2) = xjac(k,2)+yl(i)*sfd(k,i)
		  djac = xjac(1,1)*xjac(2,2) - xjac(1,2)*xjac(2,1)
		  xjaci(1,1) = xjac(2,2)/djac
		  xjaci(1,2) = -xjac(1,2)/djac
		  xjaci(2,1) = -xjac(2,1)/djac
		  xjaci(2,2) = xjac(1,1)/djac
		  da = djac*wg(ix)*wg(iy)
		  do 80 i =1,2
		  do 80 j =1,8
80        sfdg(i,j) =0.0
		  do 90 i =1,2
		  do 90 k =1,8
		  do 90 j =1,2
90        sfdg(i,k) = sfdg(i,k) +xjaci(i,j)*sfd(j,k)
		  do 100 i =1,5
		  do 100 j =1,24
100       b(i,j)=0.0
		  do 110 i =1,8
		  k1 = 3*(i-1)+1
		  k2 = k1+1
		  k3 = k2+1
		  b(1,k3) = sfdg(1,i)
		  b(2,k2)  = -sfdg(2,i)
		  b(3,k3) = sfdg(2,i)
		  b(3,k2) = - sfdg(1,i)
		  b(4,k1) = sfdg(1,i)
		  b(4,k3) = sf(i)
		  b(5,k1) = sfdg(2,i)
		  b(5,k2) = -sf(i)
 110       continue
		  do 115 i =  1,5
		  do 115 j =  1,24
 115       db(i,j) = 0.0
		  do 120 i =1,5
		  do 120 j =1,24
		  do 120 k =1,5
 120       db(i,j) = db(i,j)+c(i,k)*b(k,j)
		  do 122 i =1,24
		  do 122 j =1,24
		  do 122 k =1,5
 122       ek(i,j)=ek(i,j)+b(k,i)*db(k,j)*da

170        continue
	   return
	   end


c**************** SUBROUTINES FOR SKYLINE TECHNIQUE******************
	subroutine columh(cht,nd,ned,neq)
       IMPLICIT double precision(A-H,O-Z)
	integer cht(neq),nd(ned)
c       cht = column height
c       nd = nodal ordinate
c       ned = degree of freedom in an element
c       neq = number of equations
c       calculates the column height
	ls = 150000
	do 30 k = 1,ned
	if (nd(k)) 10,30,10
10      if (nd(k) - ls) 20,30,30
20      ls=nd(k)
30      continue
	do 40 k=1,ned
	ii=nd(k)
	if(ii.eq.0) go to 40
	me=ii - ls
	if(me .gt. cht(ii))cht(ii)=me
40      continue
	return
	end
c******************************************************
	subroutine cadnum(cht,NDS,neq,neq1,nsky,mband)
       IMPLICIT double precision(A-H,O-Z)
	integer cht(neq),NDS(neq1)
c       cht = column hgt
c       NDS = address of the column(i.e. diagnol elements)
c       neq = number of equation
c       neq1 = neq + 1
c       it computes the address of diagnol element assuming
c       column height is known
	do 10 i=1,neq1
10      NDS(i)=0.0
	NDS(1) = 1
	NDS(2) = 2
	mband = 0
	if(neq .eq. 1) go to 30
	do 20 i = 2,neq
	if(cht(i) .gt. mband) mband = cht(i)
20      NDS(i+1) = NDS(i) + cht(i) + 1
30      mband=mband+1
	nsky = NDS(neq1) - 1
	return
	end
c*************************************************************
	subroutine passem(sk,ek,NDS,nd,ned,neq1,nsky,nued)
       IMPLICIT double precision(A-H,O-Z)
	dimension NDS(neq1),nd(ned),ek(nued,nued)
	dimension sk(nsky)
c       assembly of element stiffness into global stiffness matrix
	do 70 i = 1,ned
	ii=nd(i)
	if(ii) 70,70,30
30      continue
	do 60 j = 1,ned
	jj = nd(j)
	if ( jj )60,60,40
40      continue
	mi = NDS(jj)
	ij = jj - ii
	if( ij )60,50,50
50      kk = mi + ij
	sk(kk)=sk(kk)+ek(i,j)
60      continue
70      continue
	return
	end



c****    here   




      subroutine s21(a,b,n)
       IMPLICIT double precision(a-h,o-z)

      dimension a(150000),b(150000)
      DO 1000 II=1,N
      a(II)=b(II)
1000  CONTINUE
      return
      end
*****************************************************************

      SUBROUTINE PASOLV(SK,P,NDS,NN,NEQ1,NSKY,inde )
      implicit double precision(a-h,o-z)
      DIMENSION SK(NSKY),P(NN),NDS(NEQ1)
      DO 140 N=1,NN
      KN=ndS(N)
      KL=KN+1
      KU=ndS(N+1)-1
      KH=KU-KL
      IF(KH) 110,90,50
50    K=N-KH
      IC=0
      KLT=KU
      DO 80 J=1,KH
      IC=IC+1
      KLT=KLT-1
      KI=ndS(K)
      nd=ndS(K+1)-KI-1
      IF(nd)80,80,60
60    KK=MIN0(IC,nd)
      C=0.0
      DO 70 L=1,KK
70    C=C+SK(KI+L)*SK(KLT+L)
      SK(KLT)=SK(KLT)-C
80    K=K+1
90    K=N
      B=0.0
      DO 100 KK=KL,KU
      K=K-1
      KI=ndS(K)
      C=SK(KK)/SK(KI)
      B=B+C*SK(KK)
100   SK(KK)=C
      SK(KN)=SK(KN)-B
c110   IF(SK(KN))120,120,140
110   if(sk(kn) .gt. -0.001) goto 140      
120   WRITE(15,222)N,SK(KN),KN
      inde=1
      return
c     STOP
140   CONTINUE
C     RETURN
C     REDUCE RIGHT HAndSIDELOADVECTOR
150   DO 180 N=1,NN
      KL=ndS(N)+1
      KU=ndS(N+1)-1
      IF(KU-KL)180,160,160
160   K=N
      C=0.0
      DO 170 KK=KL,KU
      K=K-1
170   C=C+SK(KK)*P(K)
      P(N)=P(N)-C
180   CONTINUE
C     BACK SUBSTITUTION
      DO 200 N=1,NN
      K=ndS(N)
200   P(N)=P(N)/SK(K)
      IF(NN .EQ. 1)RETURN
      N=NN
      DO 230 L=2,NN
      KL=ndS(N)+1
      KU=ndS(N+1)-1
      IF(KU-KL)230,210,210
210   K=N
      DO 220 KK=KL,KU
      K=K-1
220   P(K)=P(K)-SK(KK)*P(N)
230   N=N-1
      RETURN
222   FORMAT(//20X,'STOP-STIFFNESS MATRIX NOT POSITIVE
     1DEFINITE',' NONPOSITIVE PIVOT FOR EQUATION',I4,//
     110X,'PIVOT=',E20.12)
      END




      subroutine gener(AC,BC,NX,NY,NEL,KP,NPER,
     1NBIG,XCO,YCO,NND,NNX,NNY)
      implicit double precision(a-h,o-z)
      DIMENSION XCO(4,8),YCO(4,8),NND(150,24),NND1(150,16),ND11(2000)
      DIMENSION N(20,20,8),ND1(2000),ND2(2000),ND3(2000),ND21(2000)
      write(*,*)ac,bc,nx,ny,x,y
      x=ac/(1.0*nx) 
      y=bc/(1.0*ny)
      do 201 i=1,4
      XCO(I,1)=0.0
      XCO(I,2)=X
      XCO(I,3)=X
      XCO(I,4)=0.0
      XCO(I,5)=X/2.0
      XCO(I,6)=X
      XCO(I,7)=X/2.0
      XCO(I,8)=0.0
      YCO(I,1)=0.0
      YCO(I,2)=0.0
      YCO(I,3)=Y
      YCO(I,4)=Y
      YCO(I,5)=0.0
      YCO(I,6)=Y/2.0
      YCO(I,7)=Y
      YCO(I,8)=Y/2.0
201   CONTINUE
      DO 100 I=1,8
100   N(1,1,I)=I
      DO 101 I=2,NY
      N(1,I,1)=N(1,I-1,4)
      N(1,I,2)=N(1,I-1,3)
      N(1,I,3)=N(1,I-1,8)+1
      N(1,I,4)=N(1,I,3)+1
      N(1,I,5)=N(1,I-1,7)
      N(1,I,6)=N(1,I,4)+1
      N(1,I,7)=N(1,I,6)+1
101   N(1,I,8)=N(1,I,7)+1
      DO 1000  K= 2,NX
      LI=7
      IF(K .EQ. 2) LI=8
      N(K,1,1)=N(K-1,1,2)
      N(K,1,2)=N(K-1,NY,LI)+1
      N(K,1,3)=N(K,1,2)+1
      N(K,1,4)=N(K-1,1,3)
      N(K,1,5)=N(K,1,3)+1
      N(K,1,6)=N(K,1,5)+1
      N(K,1,7)=N(K,1,6)+1
      N(K,1,8)=N(K-1,1,6)
      DO 1000 J=2,NY
      N(K,J,1)=N(K-1,J,2)
      N(K,J,2)=N(K,J-1,3)
      N(K,J,3)=N(K,J-1,7)+1
      N(K,J,4)=N(K-1,J,3)
      N(K,J,5)=N(K,J-1,7)
      N(K,J,6)=N(K,J,3)+1
      N(K,J,7)=N(K,J,6)+1
      N(K,J,8)=N(K-1,J,6)
1000  CONTINUE
      KOUNT=0
      ND1(1)=0
      ND2(1)=1*KP
      ND3(1)=2*KP
      KOUNT=1+(1-KP)+(1-KP)
      KLM=1
      DO 30 I=1,NX
      DO 30 J=1,NY
      DO 30 K=1,8
      IF(N(I,J,K) .LE. KLM)GO TO 30
      KLM=N(I,J,K)
      IF((I.EQ.1.AND.J.EQ.1.AND.K.EQ.1).OR.
     1(I.EQ.NX.AND.J.EQ.1.AND.K.EQ.2).OR.
     1(I.EQ.1.AND.J.EQ.NY.AND.K.EQ.4).OR.
     1(I.EQ.NX.AND.J.EQ.NY.AND.K.EQ.3))GO TO 72
      IF((I.EQ.1.AND.(K.EQ.1.OR.K.EQ.8.OR.K.EQ.4)).OR.
     1(I.EQ.NX.AND.(K.EQ.2.OR.K.EQ.6.OR.K.EQ.3)))GO TO 40
      IF((J.EQ.1.AND.(K.EQ.1.OR.K.EQ.5.OR.K.EQ.2)).OR.
     1(J.EQ.NY.AND.(K.EQ.3.OR.K.EQ.7.OR.K.EQ.4)))GO TO 50
      GO TO 60
72    ND1(KLM)=0
      ND2(KLM)=((KLM*3-3)+1-KOUNT)*KP
      ND3(KLM)=((KLM*3-3)+2-KOUNT)*KP
      KOUNT=KOUNT+1+(1-KP)+(1-KP)
      GO TO 30
40    ND1(KLM)=0
      ND2(KLM)=0
      ND3(KLM)=((KLM*3-3)+1-KOUNT)*KP
      KOUNT=KOUNT+2+(1-KP)
      GO TO 30
50    ND1(KLM)=0
      ND2(KLM)=((KLM*3-3)+1-KOUNT)*KP
      ND3(KLM)=0
      KOUNT=KOUNT+2+(1-KP)
      GO TO 30
60    ND1(KLM)=(KLM*3-3)+1-KOUNT
      ND2(KLM)=(KLM*3-3)+2-KOUNT
      ND3(KLM)=(KLM*3-3)+3-KOUNT
30    CONTINUE
      DO 601 I =1,NX
      DO 601 J=1,NY
      NUMB=(I-1)*NY+J
      DO 602 K=1,8
      NAN = N(I,J,K)
      K1=(K*3-3)+1
      NND(NUMB,K1)=ND1(NAN)
      NND(NUMB,K1+1)=ND2(NAN)
      NND(NUMB,K1+2)=ND3(NAN)
602   CONTINUE
601   CONTINUE
      NEL = NX*NY
      NBIG=ND1(1)
      DO 999 I=1,KLM
      IF(ND1(I).LT.NBIG)GO TO 9991
      NBIG=ND1(I)
9991  IF(ND2(I).LT.NBIG)GO TO 9992
      NBIG=ND2(I)
9992  IF(ND3(I).LT.NBIG)GO TO 999
      NBIG=ND3(I)
999   CONTINUE

	KP1=1
	KOUNT1=0
	KLM1=1
	DO 130 I=1,NX
	DO 130 J=1,NY
	DO 130 K=1,8
	IF(N(I,J,K) .LT. KLM1)GO TO 130
	KLM1=N(I,J,K)
	IF(I.EQ.1.AND.(K.EQ.1.OR.K.EQ.8.OR.K.EQ.4))GOTO 172
160      nd11(KLM1)=(KLM1*2-2)+1-KOUNT1
	nd21(KLM1)=(KLM1*2-2)+2-KOUNT1
	GOTO 130
172      nd11(KLM1)=((KLM1*2-2)+1-KOUNT1)*KP1
	nd21(KLM1)=((KLM1*2-2)+2-KOUNT1)*KP1
	KOUNT1=KOUNT1+(1-KP1)+(1-KP1)
130      CONTINUE
	nbig1=0
	DO 1601 I =1,NX
	DO 1601 J=1,NY
	NUMB1=(I-1)*NY+J
	DO 1602 K=1,8
	NAN1 = N(I,J,K)
	K11=K*2-1
	nnd1(NUMB1,K11)=nd11(NAN1)
	nnd1(NUMB1,K11+1)=nd21(NAN1)
	IF(nnd1(numb1,k11).LT.NBIG1)GO TO 19991
	NBIG1=nnd1(numb1,k11)
19991    IF(nnd1(numb1,k11+1).LT.NBIG1)GO TO 1999
	 NBIG1=nnd1(numb1,k11+1)
1999     CONTINUE
1602     CONTINUE
1601     CONTINUE
	NEL = NX*NY
1112  format(2x,17(i3,1x))
1114  format(2x,17(i3,1x))
      RETURN
      END
