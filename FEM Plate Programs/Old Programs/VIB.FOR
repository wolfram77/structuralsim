c       this is a program to find non-dimensional frequencies
c       of free vibration of plates 


c*************************  reading all datas ***********************
	implicit double precision(a-h,o-z)
	integer cht(2000),nds(2000),nd(24)
	DIMENSION EKK(24,24),EM(24,24),XL(8),YL(8) 
	DIMENSION XCO(4,8),YCO(4,8),NND(150,24)
	DIMENSION a(150000),b(150000) 
	DIMENSION CA(150000),CB(150000)
	DIMENSION R(2000,5),TT(2000),AR(20),BR(20)
	DIMENSION VEC(5,5),D(5),RTOLV(5)
	DIMENSION W(2000),eigv1(2)
	open(unit=10,file='vib.in')
	open(unit=20,file='vib.out')
	kount=0
	read(10,*)alen,bre,th,e,pr,nx,ny,kp
	call gener(alen,bre,nx,ny,nel,kp,nbig,xco,yco,
     1  nnd,nbig1)
	do 850 j=1,8
	xl(j)=xco(1,j)
	yl(j)=yco(1,j)
	write(*,*)xl(j),yl(j)
850     continue
	call bstif(EKK,XL,YL,TH,PR,E)
	call mass(EM,XL,YL,TH,row) 
	write(20,*)'element mass'
	write(20,*)(em(i,i),i=1,24)

c************************      LOOP6     ******************************
	NSKY=0
	DO 11 I=1,nbig
	CHT(I)=0
11      NDS(I)=0
	MBAND=0
	DO 1101 LNUM=1,NEL
	DO 1301 I=1,24
1301    ND(I)=NND(LNUM,I)
	CALL COLUMH(CHT,ND,24,NBIG)
1101    CONTINUE
	DO 1102 LNUM=1,NEL
	NDO1=NBIG+1
	CALL CADNUM(CHT,NDS,NBIG,NDO1,NSKY,MBAND)
1102    CONTINUE
	write(*,*)nsky
	do 970 i=1,nsky
	a(i)=0.0
	b(i)=0.0
970     continue
	do 10897 i=1,24
10897   write(20,10898)(ekk(i,j),j=1,6)
10898   format(6(f12.6,1x))
	do 1200 lnum=1,nel
	DO 5301 I=1,24
5301    ND(I)=NND(LNUM,I)
	do 419 i=1,8
	xl(i)=xco(1,i)
419     yl(i)=yco(1,i)
	CALL PASSEM (A,EKK,NDS,ND,24,NBIG,NSKY,24)
	call passem (B,EM ,NDS,ND,24,NBIG,NSKY,24)
1200    continue
9600    format(2x,i3,2x,2(e15.7,2x))
	nwk=nsky
	nn=nbig
	call s21(ca,a,nwk)
	call s21(cb,b,nwk)
	call s22(nrukku,nnm,nwm,nroot,rtol,nc,nnc,nitem,ifss,ifpr,
     1  nbig,nsky)
	CALL SUBSPACE(ca,cb,NDS,R,EIGV1,
     1  TT,W,AR,BR,VEC,D,RTOLV
     1  ,NN,NNM,NWK,NWM,NROOT,RTOL,NC,NNC,NITEM,IFSS,IFPR
     1  ,NRUKKU)
	ei1=eigv1(1)
	ans1=dsqrt(ei1)
	an1=ans1*bre*bre*dsqrt(row*th/((e*th**3)/(12.0*(1.0-pr*pr))))
	ei2=eigv1(2)
	ans2=dsqrt(ei2)
	an2=ans2*bre*bre*dsqrt(row*th/((e*th**3)/(12.0*(1.0-pr*pr))))
	write(*,*)an1,an2
	stop
	end  




      subroutine gener(AC,BC,NX,NY,NEL,KP,
     1NBIG,XCO,YCO,NND,NBIG1)
      implicit double precision(a-h,o-z)
      DIMENSION XCO(4,8),YCO(4,8),NND(150,24),NND1(150,16),ND11(2000)
      DIMENSION N(20,20,8),ND1(2000),ND2(2000),ND3(2000),ND21(2000)
      write(*,*)ac,bc,nx,ny,x,y
      x=ac/(1.0*nx) 
      y=bc/(1.0*ny)
      do 201 i=1,4
      XCO(I,1)=0.0
      XCO(I,2)=X
      XCO(I,3)=X
      XCO(I,4)=0.0
      XCO(I,5)=X/2.0
      XCO(I,6)=X
      XCO(I,7)=X/2.0
      XCO(I,8)=0.0
      YCO(I,1)=0.0
      YCO(I,2)=0.0
      YCO(I,3)=Y
      YCO(I,4)=Y
      YCO(I,5)=0.0
      YCO(I,6)=Y/2.0
      YCO(I,7)=Y
      YCO(I,8)=Y/2.0
201   CONTINUE
      DO 100 I=1,8
100   N(1,1,I)=I
      DO 101 I=2,NY
      N(1,I,1)=N(1,I-1,4)
      N(1,I,2)=N(1,I-1,3)
      N(1,I,3)=N(1,I-1,8)+1
      N(1,I,4)=N(1,I,3)+1
      N(1,I,5)=N(1,I-1,7)
      N(1,I,6)=N(1,I,4)+1
      N(1,I,7)=N(1,I,6)+1
101   N(1,I,8)=N(1,I,7)+1
      DO 1000  K= 2,NX
      LI=7
      IF(K .EQ. 2) LI=8
      N(K,1,1)=N(K-1,1,2)
      N(K,1,2)=N(K-1,NY,LI)+1
      N(K,1,3)=N(K,1,2)+1
      N(K,1,4)=N(K-1,1,3)
      N(K,1,5)=N(K,1,3)+1
      N(K,1,6)=N(K,1,5)+1
      N(K,1,7)=N(K,1,6)+1
      N(K,1,8)=N(K-1,1,6)
      DO 1000 J=2,NY
      N(K,J,1)=N(K-1,J,2)
      N(K,J,2)=N(K,J-1,3)
      N(K,J,3)=N(K,J-1,7)+1
      N(K,J,4)=N(K-1,J,3)
      N(K,J,5)=N(K,J-1,7)
      N(K,J,6)=N(K,J,3)+1
      N(K,J,7)=N(K,J,6)+1
      N(K,J,8)=N(K-1,J,6)
1000  CONTINUE
      KOUNT=0
      ND1(1)=0
      ND2(1)=1*KP
      ND3(1)=2*KP
      KOUNT=1+(1-KP)+(1-KP)
      KLM=1
      DO 30 I=1,NX
      DO 30 J=1,NY
      DO 30 K=1,8
      IF(N(I,J,K) .LE. KLM)GO TO 30
      KLM=N(I,J,K)
      IF((I.EQ.1.AND.J.EQ.1.AND.K.EQ.1).OR.
     1(I.EQ.NX.AND.J.EQ.1.AND.K.EQ.2).OR.
     1(I.EQ.1.AND.J.EQ.NY.AND.K.EQ.4).OR.
     1(I.EQ.NX.AND.J.EQ.NY.AND.K.EQ.3))GO TO 72
      IF((I.EQ.1.AND.(K.EQ.1.OR.K.EQ.8.OR.K.EQ.4)).OR.
     1(I.EQ.NX.AND.(K.EQ.2.OR.K.EQ.6.OR.K.EQ.3)))GO TO 40
      IF((J.EQ.1.AND.(K.EQ.1.OR.K.EQ.5.OR.K.EQ.2)).OR.
     1(J.EQ.NY.AND.(K.EQ.3.OR.K.EQ.7.OR.K.EQ.4)))GO TO 50
      GO TO 60
72    ND1(KLM)=0
      ND2(KLM)=((KLM*3-3)+1-KOUNT)*KP
      ND3(KLM)=((KLM*3-3)+2-KOUNT)*KP
      KOUNT=KOUNT+1+(1-KP)+(1-KP)
      GO TO 30
40    ND1(KLM)=0
      ND2(KLM)=0
      ND3(KLM)=((KLM*3-3)+1-KOUNT)*KP
      KOUNT=KOUNT+2+(1-KP)
      GO TO 30
50    ND1(KLM)=0
      ND2(KLM)=((KLM*3-3)+1-KOUNT)*KP
      ND3(KLM)=0
      KOUNT=KOUNT+2+(1-KP)
      GO TO 30
60    ND1(KLM)=(KLM*3-3)+1-KOUNT
      ND2(KLM)=(KLM*3-3)+2-KOUNT
      ND3(KLM)=(KLM*3-3)+3-KOUNT
30    CONTINUE
      DO 601 I =1,NX
      DO 601 J=1,NY
      NUMB=(I-1)*NY+J
      DO 602 K=1,8
      NAN = N(I,J,K)
      K1=(K*3-3)+1
      NND(NUMB,K1)=ND1(NAN)
      NND(NUMB,K1+1)=ND2(NAN)
      NND(NUMB,K1+2)=ND3(NAN)
602   CONTINUE
601   CONTINUE
      NEL = NX*NY
      NBIG=ND1(1)
      DO 999 I=1,KLM
      IF(ND1(I).LT.NBIG)GO TO 9991
      NBIG=ND1(I)
9991  IF(ND2(I).LT.NBIG)GO TO 9992
      NBIG=ND2(I)
9992  IF(ND3(I).LT.NBIG)GO TO 999
      NBIG=ND3(I)
999   CONTINUE

	KP1=1
	KOUNT1=0
	KLM1=1
	DO 130 I=1,NX
	DO 130 J=1,NY
	DO 130 K=1,8
	IF(N(I,J,K) .LT. KLM1)GO TO 130
	KLM1=N(I,J,K)
	IF(I.EQ.1.AND.(K.EQ.1.OR.K.EQ.8.OR.K.EQ.4))GOTO 172
160      nd11(KLM1)=(KLM1*2-2)+1-KOUNT1
	nd21(KLM1)=(KLM1*2-2)+2-KOUNT1
	GOTO 130
172      nd11(KLM1)=((KLM1*2-2)+1-KOUNT1)*KP1
	nd21(KLM1)=((KLM1*2-2)+2-KOUNT1)*KP1
	KOUNT1=KOUNT1+(1-KP1)+(1-KP1)
130      CONTINUE
	nbig1=0
	DO 1601 I =1,NX
	DO 1601 J=1,NY
	NUMB1=(I-1)*NY+J
	DO 1602 K=1,8
	NAN1 = N(I,J,K)
	K11=K*2-1
	nnd1(NUMB1,K11)=nd11(NAN1)
	nnd1(NUMB1,K11+1)=nd21(NAN1)
	IF(nnd1(numb1,k11).LT.NBIG1)GO TO 19991
	NBIG1=nnd1(numb1,k11)
19991    IF(nnd1(numb1,k11+1).LT.NBIG1)GO TO 1999
	 NBIG1=nnd1(numb1,k11+1)
1999     CONTINUE
1602     CONTINUE
1601     CONTINUE
	NEL = NX*NY
1112  format(2x,17(i3,1x))
1114  format(2x,17(i3,1x))
      RETURN
      END
	    subroutine bstif(ek,xl,yl,th,pr,e)
       IMPLICIT double precision(A-H,O-Z)
	    dimension ek(24,24),c(5,5),xl(8),yl(8),r(8),s(8)
	  dimension xjaci(2,2),sfdg(2,8),sf(8),b(5,24),xjac(2,2)
	   dimension db(5,24),gp(2),wg(2),sfd(2,8)
	     data wg/1.D0,1.D0/
	     data gp/-0.5773502691896,0.5773502691896/
	     data r/-1.D0,1.D0,1.D0,-1.D0,0.D0,1.D0,0.D0,-1.D0/
	     data s/-1.D0,-1.D0,1.D0,1.D0,-1.D0,0.D0,1.D0,0.D0/
	     do 1657 i=1,5
	     do 1657 j=1,5
1657         c(i,j)=0.0
	    c(1,1) = e*th**3/(12.0*(1.0-pr**2))
	     c(1,2) = c(1,1)*pr
	     c(2,1) = c(1,2)
	     c(2,2) = c(1,1)
	     c(3,3) = c(1,1)*(1.0-pr)/2.0
c            c(4,4)=70000.0   > 5000.0
c            c(4,4) = e*th**3/(12.0*(1.0-pr**2))*35000.0/bre**2
c            c(4,4) = e*th/(2.0*(1.0+pr))  ~ 1800.0
	     c(4,4) = e*th/(2.0*(1.0+pr))
	     c(5,5) = c(4,4)
C            INTIALIZE STIFFNESS AND STABILITY MATRIX
	     do 20 i = 1,24
	     do 20 j = 1,24
20           ek(i,j) = 0.0
C            COMPUTE THE SS-MATRIX
C         ENTER THE LOOP FOR INTEGRATION
		  do 170 ix =1,2
		  do 170 iy =1,2
C         CALUCALATE SHAPE FUNCTIONS AND THEIR DERVATIVES
		  do 50 i =1,8
		  aa = (1.0 + r(i)*gp(ix))
		  bb = (1.0 + s(i)*gp(iy))
		  if (i .gt. 4)  goto 40
		  sf(i) = 0.25*aa*bb*(aa+bb-3.0)
		  sfd(1,i) = 0.25*bb*(2.0*aa+bb-3.0)*r(i)
		  sfd(2,i) = 0.25*aa*(2.0*bb+aa-3.0)*s(i)
		  goto 50
40        aa = aa + (r(i)**2-1.0)*gp(ix)**2
		  bb = bb + (s(i)**2-1.0)*gp(iy)**2
		  sf(i) = 0.5*aa*bb
		  sfd(1,i) = 0.5*(r(i)+2.0*(r(i)**2-1.0)*gp(ix))*bb
		  sfd(2,i) = 0.5*(s(i)+2.0*(s(i)**2-1.0)*gp(iy))*aa
50               continue
		  do 60 i =1,2
		  do 60 j =1,2
60        xjac(i,j) = 0.0
		  do 70 i =1,8
		  do 70 k =1,2
		  xjac(k,1) = xjac(k,1)+xl(i)*sfd(k,i)
70        xjac(k,2) = xjac(k,2)+yl(i)*sfd(k,i)
		  djac = xjac(1,1)*xjac(2,2) - xjac(1,2)*xjac(2,1)
		  xjaci(1,1) = xjac(2,2)/djac
		  xjaci(1,2) = -xjac(1,2)/djac
		  xjaci(2,1) = -xjac(2,1)/djac
		  xjaci(2,2) = xjac(1,1)/djac
		  da = djac*wg(ix)*wg(iy)
		  do 80 i =1,2
		  do 80 j =1,8
80        sfdg(i,j) =0.0
		  do 90 i =1,2
		  do 90 k =1,8
		  do 90 j =1,2
90        sfdg(i,k) = sfdg(i,k) +xjaci(i,j)*sfd(j,k)
		  do 100 i =1,5
		  do 100 j =1,24
100       b(i,j)=0.0
		  do 110 i =1,8
		  k1 = 3*(i-1)+1
		  k2 = k1+1
		  k3 = k2+1
		  b(1,k3) = sfdg(1,i)
		  b(2,k2)  = -sfdg(2,i)
		  b(3,k3) = sfdg(2,i)
		  b(3,k2) = - sfdg(1,i)
		  b(4,k1) = sfdg(1,i)
		  b(4,k3) = sf(i)
		  b(5,k1) = sfdg(2,i)
		  b(5,k2) = -sf(i)
 110       continue
		  do 115 i =  1,5
		  do 115 j =  1,24
 115       db(i,j) = 0.0
		  do 120 i =1,5
		  do 120 j =1,24
		  do 120 k =1,5
 120       db(i,j) = db(i,j)+c(i,k)*b(k,j)
		  do 122 i =1,24
		  do 122 j =1,24
		  do 122 k =1,5
 122       ek(i,j)=ek(i,j)+b(k,i)*db(k,j)*da

170        continue
	   return
	   end

	     subroutine mass(em,xl,yl,th,ROW)
	      IMPlicit double precision(a-h,o-z)
	     dimension em(24,24),ss(6,6),xl(8),yl(8),r(8),s(8)
	     dimension sf(8),sfd(2,8),xjac(2,2)
	     dimension xjaci(2,2),sfdg(2,8),g(6,24),gp(2),wg(2),sg(6,24)
	     data wg/1.D0,1.D0/
	     data gp/-0.5773502691896,0.5773502691896/
	     data r/-1.D0,1.D0,1.D0,-1.D0,0.D0,1.D0,0.D0,-1.D0/
	     data s/-1.D0,-1.D0,1.D0,1.D0,-1.D0,0.D0,1.D0,0.D0/
	     do 20 i = 1,24
	     do 20 j = 1,24
20           em(i,j) = 0.0
	     spwt=0.028e-03
	     row=spwt/9810.0     
	   row=7800.0  
C            COMPUTE THE SS-MATRIX

	     do 21 i =1,3
	     do 21 j = 1,3
21           ss(i,j)=0.0
	     ss(1,1)=ROW *th
	     ss(2,2)=ROW*TH*TH/12.0 *th
	     ss(3,3)=ROW*TH*TH/12.0 *th

C            ENTER THE LOOP FOR INTEGRATION

	     do 170 ix =1,2
	     do 170 iy =1,2

C            CALUCALATE SHAPE FUNCTIONS AND THEIR DERVATIVES

	     do 50 i =1,8
	     aa = (1.0 + r(i)*gp(ix))
	     bb = (1.0 + s(i)*gp(iy))
	     if (i .gt. 4)  goto 40
	     sf(i) = 0.25*aa*bb*(aa+bb-3.0)
	     sfd(1,i) = 0.25*bb*(2.0*aa+bb-3.0)*r(i)
	     sfd(2,i) = 0.25*aa*(2.0*bb+aa-3.0)*s(i)
	     goto 50
40           aa = aa + (r(i)**2-1.0)*gp(ix)**2
	     bb = bb + (s(i)**2-1.0)*gp(iy)**2
	     sf(i) = 0.5*aa*bb
	     sfd(1,i) = 0.5*(r(i)+2.0*(r(i)**2-1.0)*gp(ix))*bb
	     sfd(2,i) = 0.5*(s(i)+2.0*(s(i)**2-1.0)*gp(iy))*aa
50           continue
	     do 60 i =1,2
	     do 60 j =1,2
60           xjac(i,j) = 0.0
	     do 70 i =1,8
	     do 70 k =1,2
	     xjac(k,1) = xjac(k,1)+xl(i)*sfd(k,i)
70           xjac(k,2) = xjac(k,2)+yl(i)*sfd(k,i)
	     djac = xjac(1,1)*xjac(2,2) - xjac(1,2)*xjac(2,1)
	     xjaci(1,1) = xjac(2,2)/djac
	     xjaci(1,2) = -xjac(1,2)/djac
	     xjaci(2,1) = -xjac(2,1)/djac
	     xjaci(2,2) = xjac(1,1)/djac
	     da = djac*wg(ix)*wg(iy)
	     do 80 i =1,2
	     do 80 j =1,8
80           sfdg(i,j) =0.0
	     do 90 i =1,2
	     do 90 k =1,8
	     do 90 j =1,2
90           sfdg(i,k) = sfdg(i,k) +xjaci(i,j)*sfd(j,k)
	     do 101 i = 1,3
	     do 101 j = 1,24
101          g(i,j) =0.0
	     do 110 i =1,8
	     k1 = 3*(i-1)+1
	     k2 = k1+1
	     k3 = k2+1
	     g(1,k1) =sf(i)
	     g(2,k2) =sf(i)
	     g(3,k3) =sf(i)
110          continue
	     do 116 i = 1,3
	     do 116 j =1,24
116          sg(i,j) = 0.0
	     do 121 i =1,3
	     do 121 j =1,24
	     do 121 k =1,3
121          sg(i,j) =sg(i,j)+ss(i,k)*g(k,j)
	     do 140 i =1,24
	     do 140 j =1,24
	     do 140 k =1,3
140          em(i,j) = em(i,j)+g(k,i)*sg(k,j)*da
170          continue
	     return
	     end



c**************** SUBROUTINES FOR SKYLINE TECHNIQUE******************
	subroutine columh(cht,nd,ned,neq)
       IMPLICIT double precision(A-H,O-Z)
	integer cht(neq),nd(ned)
c       cht = column height
c       nd = nodal ordinate
c       ned = degree of freedom in an element
c       neq = number of equations
c       calculates the column height
	ls = 150000
	do 30 k = 1,ned
	if (nd(k)) 10,30,10
10      if (nd(k) - ls) 20,30,30
20      ls=nd(k)
30      continue
	do 40 k=1,ned
	ii=nd(k)
	if(ii.eq.0) go to 40
	me=ii - ls
	if(me .gt. cht(ii))cht(ii)=me
40      continue
	return
	end
c******************************************************
	subroutine cadnum(cht,NDS,neq,neq1,nsky,mband)
       IMPLICIT double precision(A-H,O-Z)
	integer cht(neq),NDS(neq1)
c       cht = column hgt
c       NDS = address of the column(i.e. diagnol elements)
c       neq = number of equation
c       neq1 = neq + 1
c       it computes the address of diagnol element assuming
c       column height is known
	do 10 i=1,neq1
10      NDS(i)=0.0
	NDS(1) = 1
	NDS(2) = 2
	mband = 0
	if(neq .eq. 1) go to 30
	do 20 i = 2,neq
	if(cht(i) .gt. mband) mband = cht(i)
20      NDS(i+1) = NDS(i) + cht(i) + 1
30      mband=mband+1
	nsky = NDS(neq1) - 1
	return
	end
c*************************************************************
	subroutine passem(sk,ek,NDS,nd,ned,neq1,nsky,nued)
       IMPLICIT double precision(A-H,O-Z)
	dimension NDS(neq1),nd(ned),ek(nued,nued)
	dimension sk(nsky)
c       assembly of element stiffness into global stiffness matrix
	do 70 i = 1,ned
	ii=nd(i)
	if(ii) 70,70,30
30      continue
	do 60 j = 1,ned
	jj = nd(j)
	if ( jj )60,60,40
40      continue
	mi = NDS(jj)
	ij = jj - ii
	if( ij )60,50,50
50      kk = mi + ij
	sk(kk)=sk(kk)+ek(i,j)
60      continue
70      continue
	return
	end





      subroutine s21(a,b,n)
       IMPLICIT double precision(a-h,o-z)

      dimension a(150000),b(150000)
      DO 1000 II=1,N
      a(II)=b(II)
1000  CONTINUE
      return
      end
      subroutine s22(nrukku,nnm,nwm,nroot,rtol,nc,nnc,
     1nitem,ifss,ifpr,nn,nwk)
       IMPLICIT double precision(a-h,o-z)
      NRUKKU=0
      NNM=NN+1
      NWM=NWK
      NROOT=1
      RTOL=1.d-05
      NC=2
      NNC=NC*(NC+1)/2
      NITEM=16
      IFSS=1
      IFPR=1
      return
      end



	SUBROUTINE SUBSPACE(A,B,NDS,R,EIGV,
     1  TT,W,AR,BR,VEC,D,RTOLV
     1  ,NN,NNM,NWK,NWM,NROOT,RTOL,NC,NNC,NITEM,IFSS,IFPR
     1  ,NRUKKU)
	IMPLICIT double precision(A-H,O-Z)
	INTEGER NDS(NNM)
	DIMENSION A(NWK),B(NWM),R(NN,NC),TT(NN),W(NN),EIGV(NC)
	DIMENSION D(NC),VEC(NC,NC),AR(NNC),BR(NNC),RTOLV(NC)
	DOUBLE PRECISION TEE,DIF
C       SQRT(X)=DSQRT(X)
C       ABS(X)=DABS(X)
C     WRITE(14,*)' THIS IS FROM SUB SPACE '
C     WRITE(14,*)' NWK ',NWK
C     WRITE(14,5100)(B(I),I=1,NWK)
C     WRITE(14,*)' STIFF '
C     WRITE(14,5100)(A(I),I=1,NWK)
5100  FORMAT(/,4(2X,E15.7))

	TOLJ=1.0D-11
C       WRITE(*,*)' TOLERANCE J ',TOLJ
	ICONV=0
	NSCH=0
	NSMAX=15
	N1=NC+1
	NC1=NC-1
C       REWIND NSTIF
C       WRITE(NSTIF) A
	DO 60 I=1,NC
60      D(I)=0.0
	ND=NN/NC
	IF(NWM.GT.NN)GO TO 4
	J=0
	DO 2 I=1,NN
	II=NDS(I)
	R(I,1)=B(I)
	IF(B(I).GT.0)J=J+1
2       W(I)=B(I)/A(II)
	IF(NC.LE.J)GO TO 16
C        WRITE(*,1007)
	STOP
4       DO 10 I=1,NN
	II=NDS(I)
	R(I,1)=B(II)
10      W(I)=B(II)/A(II)
16      DO 20 J=2,NC
	DO 20 I=1,NN
20      R(I,J)=0.
	L=NN-ND
	DO 30 J =2,NC
	RT=0.
	DO 40 I=1,L
	IF(W(I).LT.RT)GO TO 40
	RT=W(I)
	IJ=I
40      CONTINUE
	DO 50 I=L,NN
	IF(W(I).LE.RT)GO TO 50
	RT=W(I)
	IJ=I
50      CONTINUE
	TT(J)=FLOAT(IJ)
	W(IJ)=0.
	L=L-ND
30      R(IJ,J)=1.0
C       WRITE(*,1008)
C       WRITE(*,1002) (TT(JA),JA=2,NC)
	ISH = 0
	CALL DECOMP(A,NDS,NN,ISH,NRUKKU)
	IF(NRUKKU .EQ. 1)RETURN
	NITE = 0
100     NITE = NITE+1
	IF(IFPR.EQ.0)GO TO 90
C       WRITE(*,1010)NITE
90      IJ = 0
	DO 110 J = 1,NC
	DO 120 K = 1,NN
120     TT(K)= R(K,J)
	CALL REDBAK(A,TT,NDS,NN)
	DO 130 I = J,NC
	ART = 0.
	DO 140 K =1,NN
140     ART=ART+R(K,I)*TT(K)
	IJ=IJ+1
130     AR(IJ)=ART
	DO 150 K=1,NN
150     R(K,J)= TT(K)
110     CONTINUE
	IJ = 0
	DO 160 J =1,NC
	CALL MULT(TT,B,R(1,J),NDS,NN,NWM)
	DO 180 I = J,NC
	BRT = 0.
	DO 190 K = 1,NN
190     BRT = BRT+R(K,I)*TT(K)
	IJ = IJ+1
180     BR(IJ)=BRT
	IF(ICONV.GT.0) GO TO 160
	DO 200 K = 1,NN
200     R(K,J) = TT(K)
160     CONTINUE
	IF(IFPR.EQ.0) GO TO 320
	IND = 1
210     CONTINUE
	II = 1
	DO 300 I = 1,NC
	ITEMP = II+NC -I
C       WRITE(*,1005)(AR(J),J=II,ITEMP)
300     II = II+N1-I
C       WRITE(*,1030)
	II = 1
	DO 310 I= 1,NC
	ITEMP = II+NC-I
C       WRITE(*,1005) (BR(J),J=II,ITEMP)
310     II = II+N1-I
	IF(IND.EQ.2) GO TO 350
320     CALL JACOBI (AR,BR,VEC,EIGV,W,NC,NNC,TOLJ,NSMAX,IFPR)
	IF(IFPR.EQ.0) GO TO 350
C       WRITE(*,1040)
	IND = 2
	GO TO 210
350     IS =0
	II=1
	DO 360 I=1,NC1
	ITEMP = II +N1 - I
	IF(EIGV(I+1).GE.EIGV(I)) GO TO 360
	IS = IS + 1
	EIGVT = EIGV(I+1)
	EIGV(I+1)=EIGV(I)
	EIGV(I)=EIGVT
	BT = BR(ITEMP)
	BR(ITEMP) = BR(II)
	BR(II)=BT
	DO 370 K=1,NC
	RT=VEC(K,I+1)
	VEC(K,I+1)=VEC(K,I)
370     VEC(K,I)=RT
360     II=ITEMP
	IF(IS.GT.0)GO TO 350
C       WRITE(*,1035)
C       WRITE(*,1006)(EIGV(I),I=1,NC)
375     DO 420  I=1,NN
	DO 422  J=1,NC
422     TT(J)=R(I,J)
	DO 424 K=1,NC
	RT=0.
	DO 430 L=1,NC
430     RT=RT+TT(L)*VEC(L,K)
424     R(I,K)=RT
420     CONTINUE
	IF(ICONV.GT.0)GO TO 500
	DO 380 I=1,NC
	tee=eigv(i)-d(i)
	DIF=ABS(tee)
380     RTOLV(I)=DIF/EIGV(I)
C       WRITE(*,1050)
C       WRITE(*,1005)(RTOLV(I),I=1,NC)
385     DO 390 I=1,NROOT
	IF(RTOLV(I) .GT. RTOL)GO TO 400
390     CONTINUE
C       WRITE(*,1060)RTOL
	ICONV=1
	GO TO 100
400     IF(NITE.LT.NITEM)GO TO 410
C       WRITE(*,1070)
	ICONV=2
	IFSS=0
	GO TO 100
410     DO 440 I=1,NC
440     D(I)=EIGV(I)
	GO TO 100
500     CONTINUE

C500     WRITE(*,1100)
C       WRITE(*,1006)(EIGV(I),I=1,NROOT)
C       WRITE(*,1110)
	RETURN
1002    FORMAT(3X,10F10.0)
1005    FORMAT(3X,10E11.4)
1006    FORMAT(3X,6E22.14)
1007    FORMAT(///,3X,'STOP NC IS LARGER THAN THE NO. OF ',
     1  'MASS DEGREE OF FREEDOM')
1008    FORMAT(3X,//,' DEGREES OF FREEDOM EXCITED BY' ,
     1  ' UNIT STARTING ITERATION VECTORS')
1010    FORMAT(3X,' ITERATION NUMBER ',I4)
1020    FORMAT(3X,' PROJECTION OF A (MATRIX AR ) ')
1030    FORMAT(3X,' PROJECTION OF B (MATRIX BR) ')
1035    FORMAT(3X,' EIGEN VALUES OF AR-LAMBDA *BR')
1040    FORMAT(3X,' AR AND BR AFTER JACOBI DIAGNOLIZATION ')
1050    FORMAT(3X,' RELATIVE TOLERANCE REACHED ON EIGEN VALUES ')
1060    FORMAT(3X,///,' CONVERGENCE REACHED FOR RTOL ',E20.8)
1070    FORMAT(3X,'*** NO CONVERGENCE IN MAXIMUM ',
     1  'NO OF ITERATION PERMITTED / WE ACCEPT '   ,
     1  'CURRENT ITERATION VALUES / THE STURN S'    ,
     1  'EQUENCE CHECK IS NOT PERFORMED ' )
1100    FORMAT(3X,///,' CALCULATED EIGEN VALYUES ARE ')
1115    FORMAT(3X,//,' PRINT ERROR NORMS ON THE EIGEN VALUES ')
1110    FORMAT(3X,//,' THE CALCULATED EIGEN VECTORS ARE ')
1120    FORMAT(3X,//,'CHECK APPLIED AT SHIFT ',E30.14)
1130    FORMAT(3X,//,' THERE ARE ,', I4,' EIGEN VALUES MISSING ')
1140    FORMAT(3X,//,' WE FOUND THE LOWEST ',I4,'EIGEN VALUES')
	END
C ****************************** SUB - SUB ROUTINES ***********
	SUBROUTINE DECOMP ( A,NDS,NN,ISH,NRUKKU)
	 IMPLICIT double precision(A-H,O-Z)
	DIMENSION A(1),NDS(1)
	DOUBLE PRECISION C
C       SQRT(X)=DSQRT(X)
C       ABS(X)=DABS(X)
	IF(NN.EQ.1)RETURN
	DO 200 N=1,NN
	KN=NDS(N)
	KL=KN+1
	KU=NDS(N+1)-1
	KH=KU-KL
	IF ( KH)304,240,210
210     K=N-KH
	IC=0
	KLT=KU
	DO 260 J = 1,KH
	IC=IC+1
	KLT = KLT -1
	KI = NDS(K)
	ND  = NDS(K+1) -KI -1
	IF(ND) 260,260,270
270     KK=MIN0(IC,ND)
	C=0.
	DO 280 L=1,KK
280     C=C+A(KI+L)*A(KLT+L)
	A(KLT)=A(KLT)-C
260     K=K+1
240     K=N
	B=0.
	DO 300 KK = KL,KU
	K=K-1
	KI=NDS(K)
	C=A(KK)/A(KI)
	IF(DABS(C).LT.1.D07)GO TO 290
	WRITE(*,2010)N,C
	NRUKKU=1
	RETURN
C       STOP
290     B=B + C*A(KK)
300     A(KK)=C
	A(KN)=A(KN)-B
304     IF(A(KN))310,310,200
310     IF(ISH.EQ.0)GO TO 320
	IF(A(KN).EQ.0.0)A(KN)=-1.0E-16
	GO TO 200
320     CONTINUE
C       WRITE(*,2000)N,A(KN)
	NRUKKU=1
	RETURN
C       STOP
200     CONTINUE
	RETURN
2000    FORMAT(//,' STOP - STIFFNESS MATRIX NOT POSITIVE',
     1  ' DEFINITE ',//,'   NON POSITIVE PIVOT FOR EQUATION',I4,
     1  //,'   PIVOT = ',E28.15)
2010    FORMAT (//,'    STOP - STURM SEQUENCE CHECK',
     1  '   FAILED BECAUSE OF',
     1  '   MULTIPLIER GROWTH FOR COLUMN NUMNER',I4,//,
     1  '   MULTIPLIER=',E20.8)
	END
C******************************************
	SUBROUTINE REDBAK (A,V,NDS,NN)
	 IMPLICIT double precision(A-H,O-Z)
	DIMENSION A(1),V(1),NDS(1)
	DO 400 N=1,NN
	KL = NDS(N) +1
	KU = NDS(N+1)-1
	IF(KU-KL)400,410,410
410     K=N
	C=0.
	DO 420 KK = KL,KU
	K=K-1
420     C=C+A(KK)*V(K)
	V(N)=V(N)-C
400     CONTINUE
	DO 480 N=1,NN
	K=NDS(N)
480     V(N)=V(N)/A(K)
	IF(NN.EQ.1)RETURN
	N=NN
	DO 500 L=2,NN
	KL=NDS(N)+1
	KU=NDS(N+1)-1
	IF(KU-KL)500,510,510
510     K=N
	DO 520 KK = KL,KU
	K=K-1
520     V(K)=V(K)-A(KK)*V(N)
500     N=N-1
	RETURN
	END
C*****************************************************
	SUBROUTINE MULT(TT,B,RR,NDS,NN,NWM)
	 IMPLICIT double precision(A-H,O-Z)
	DIMENSION TT(1),B(1),RR(1),NDS(1)
	IF(NWM.GT.NN) GO TO 20
	DO 10 I = 1,NN
10      TT(I)=B(I)*RR(I)
	RETURN
20      DO 40 I=1,NN
40      TT(I)=0.
	DO 100 I =1,NN
	KL = NDS(I)
	KU = NDS (I+1)-1
	II = I + 1
	CC = RR( I)
	 DO 100 KK = KL,KU
	II = II - 1
100     TT(II)=TT(II)+B(KK)*CC
	IF (NN.EQ.1)RETURN
	DO 200 I = 2,NN
	KL = NDS(I)+1
	KU = NDS(I+1)-1
	IF (KU-KL)200,210,210
210     II=I
	AA=0.
	DO 220 KK =KL,KU
	II = II-1
220     AA = AA+B(KK)*RR(II)
	TT(I)=TT(I)+AA
200     CONTINUE
	RETURN
	END

C***************************************************************
	SUBROUTINE SCHECK(EIGV,RTOLV,BUP,
     1  BLO,BUPC,NEIV,NC,NEI,RTOL,SHIFT)
	 IMPLICIT double precision(A-H,O-Z)
	DIMENSION EIGV(NC),RTOLV(NC),BUP(NC),BLO(NC),BUPC(NC),NEIV(NC)
	FTOL=0.01
	DO 100 I =1,NC
	BUP(I)=EIGV(I)*(1.+FTOL)
100     BLO(I)=EIGV(I)*(1.-FTOL)
	NROOT=0
	DO 120 I = 1,NC
120     IF(RTOLV(I).LT.RTOL)NROOT = NROOT+1
	IF(NROOT.GE.1)GO TO 200
C       WRITE(*,1010)
	STOP
200     DO 240 I = 1,NROOT
240     NEIV(I)=1
	IF(NROOT.NE.1)GO TO 260
	BUPC(1)=BUP(1)
	LM=1
	L=1
	I=2
	GO TO 295
260     L=1
	I=2
270     IF(BUP(I-1).LE.BLO(I)) GO TO 280
	NEIV(L)=NEIV(L)+1
	I=I+1
	IF(I.LE.NROOT)GO TO 270
280     BUPC(L)=BUP(I-1)
	IF(I.GT.NROOT) GO TO 290
	L=L+1
	I=I+1
	IF(I.LE.NROOT)GO TO 270
	BUPC(L)=BUP(I-1)
290     LM=L
	IF(NROOT.EQ.NC) GO TO 300
295     IF(BUP(I-1).LE.BLO(I))GO TO 300
	IF(RTOLV(I).GT.RTOL)GO TO 300
	BUPC(L)=BUP(I)
	NEIV(L)=NEIV(L)+1
	NROOT=NROOT+1
	IF(NROOT.EQ.NC)GO TO 300
	I=I+1
	GO TO 295
300     CONTINUE
C00     WRITE(*,1020)
C       WRITE(*,1005)(BUPC(I),I=1,LM)
C       WRITE(*,1030)
C       WRITE(*,1006)(NEIV(I),I=1,LM)
	LL=LM-1
	IF(LM.EQ.1)GO TO 310
330     DO 320 I= 1,LL
320     NEIV(L)=NEIV(L)+NEIV(I)
	L=L-1
	LL=LL-1
	IF(L.NE.1)GO TO 330
310     WRITE(*,1040)
C       WRITE(*,1006)(NEIV(I),I=1,LM)
	L=0
	DO 340 I=1,LM
	L=L+1
	IF(NEIV(I).GE.NROOT)GO TO 350
340     CONTINUE
350     SHIFT=BUPC(L)
	NEI=NEIV(L)
	RETURN
1005    FORMAT(1H0,6E22.14)
1006    FORMAT(1H0,6I22)
1010    FORMAT('   ERROR SOLN.. STOP'/)
1020    FORMAT('   UPPER BOUNDS ON CLUSTERS '/)
1030    FORMAT('   NO.. OF EIGV VALUES / CLUSTER '/)
1040    FORMAT('   EIGV VALUES < UPPER BOUNDS  '/)
	END
C      **********************************
	SUBROUTINE  JACOBI(A,B,X,EIGV,D,N,NWA,RTOL,NSMAX,IFPR)
	IMPlicit double precision (a-h,o-z)
	DIMENSION A(NWA),B(NWA),X(N,N),EIGV(N),D(N)
	DOUBLE PRECISION SQCH,CHECK,D2,D1,DIF,FEE,BB,HEE
C       ABS(Y)=DABS(Y)
C       SQRT(Y)=DSQRT(Y)
	N1=N+1
	II=1
	DO 10 I = 1,N
	IF(A(II).GT.0.AND.B(II).GT.0)GO TO 4
	WRITE(*,2020)II,A(II),B(II)
	NRUKKU=1
	RETURN
4       D(I)=A(II)/B(II)
	EIGV(I)=D(I)
10      II=II+N1-I
	DO 30 I=1,N
	DO 20 J=1,N
20      X(I,J)=0.0
30      X(I,I)=1.
	IF(N.EQ.1)RETURN
	NSWEEP=0
	NR = N-1
40      NSWEEP=NSWEEP+1
C       IF(IFPR.EQ.1)WRITE(*,2000)NSWEEP
	LPAL=1
	IF(IFPR.EQ.1)LPAL=1
	EPS = (.01**NSWEEP)**2
	DO 210 J=1,NR
	JP1=J+1
	JM1=J-1
	LJK=JM1*N - JM1*J/2
	JJ = LJK+J
	DO 210 K = JP1,N
	KP1 = K+1
	KM1 = K-1
	JK = LJK+K
	KK = KM1*N-KM1*K/2 + K
	EPTOLA=(A(JK)*A(JK))/(A(JJ)*A(KK))
	EPTOLB=(B(JK)*B(JK))/(B(JJ)*B(KK))
	IF((EPTOLA.LT.EPS).AND.(EPTOLB.LT.EPS)) GO TO 210
	AKK = A(KK)*B(JK)-B(KK)*A(JK)
	AJJ = A(JJ)*B(JK)-B(JJ)*A(JK)
	AB=A(JJ)*B(KK)-A(KK)*B(JJ)
	CHECK=(AB*AB+4.0*AKK*AJJ)/4.0
	IF(CHECK)50,60,60
50      CONTINUE
C       WRITE(*,2020)
	STOP
60      SQCH=DSQRT(CHECK)
	D1=AB/2.0+SQCH
	D2=AB/2.0-SQCH
	DEN=D1
	IF(DABS(D2).GT.DABS(D1))DEN=D2
	IF(DEN)80,70,80
70      CA=0.
	CG=-A(JK)/A(KK)
	GO TO 90
80      CA=AKK/DEN
	CG=-AJJ/DEN
90      IF(N-2)100,190,100
100     IF(JM1-1)130,110,110
110     DO 120 I= 1,JM1
	IM1=I-1
	IJ=IM1*N-IM1*I/2+J
	IK=IM1*N-IM1*I/2+K
	AJ=A(IJ)
	BJ=B(IJ)
	AK=A(IK)
	BK=B(IK)
	A(IJ)=AJ+CG*AK
	B(IJ)=BJ+CG*BK
	A(IK)=AK+CA*AJ
120     B(IK)=BK+CA*BJ
130     IF(KP1-N)140,140,160
140     LJI=JM1*N-JM1*J/2
	LKI=KM1*N-KM1*K/2
	DO 150 I= KP1,N
	JI=LJI+I
	KI = LKI+I
	AJ=A(JI)
	BJ=B(JI)
	AK=A(KI)
	BK=B(KI)
	A(JI)=AJ+CG*AK
	B(JI)=BJ+CG*BK
	A(KI)=AK+CA*AJ
150     B(KI)=BK+CA*BJ
160     IF(JP1-KM1)170,170,190
170     LJI=JM1*N-JM1*J/2
	DO 180 I=JP1,KM1
	JI=LJI+I
	IM1=I-1
	IK=IM1*N-IM1*I/2+K
	AJ=A(JI)
	BJ=B(JI)
	AK=A(IK)
	BK=B(IK)
	A(JI)=AJ+CG*AK
	B(JI)=BJ+CG*BK
	A(IK)=AK+CA*AJ
180     B(IK)=BK+CA*BJ
190     AK=A(KK)
	BK=B(KK)
	A(KK)=AK+2.*CA*A(JK)+CA*CA*A(JJ)
	B(KK)=BK+2.*CA*B(JK)+CA*CA*B(JJ)
	A(JJ)=A(JJ)+2.0*CG*A(JK)+CG*CG*AK
	B(JJ)=B(JJ)+2.0*CG*B(JK)+CG*CG*BK
	A(JK)=0.
	B(JK)=0.
	DO 200 I=1,N
	XJ=X(I,J)
	XK=X(I,K)
	X(I,J)=XJ+CG*XK
200     X(I,K)=XK+CA*XJ
210     CONTINUE
	II=1
	DO 220 I=1,N
	IF(A(II).GT.0. .AND. B(II).GT.0.)GO TO 215
C       WRITE(*,2020)II,A(II),B(II)
	STOP
215     EIGV(I)=A(II)/B(II)
220     II=II+N1-I
C      WRITE(*,2030)
C      WRITE(*,2010)(EIGV(I),I=1,N)
230     DO 240 I=1,N
	TOL=RTOL*D(I)
	FEE=EIGV(I)-D(I)
	DIF=DABS(FEE)
	IF(DIF.GT.TOL)GO TO 280
240     CONTINUE
	EPS=RTOL**2
	DO 250 J=1,NR
	JM1=J-1
	JP1=J+1
	LJK=JM1*N-JM1*J/2
	JJ=LJK+J
	DO 250 K=JP1,N
	KM1=K-1
	JK=LJK+K
	KK=KM1*N-KM1*K/2+K
	EPSA=(A(JK)*A(JK))/(A(JJ)*A(KK))
	EPSB=(B(JK)*B(JK))/(B(JJ)*B(KK))
	IF ((EPSA.LT.EPS).AND.(EPSB.LT.EPS))GO TO 250
	GO TO 280
250     CONTINUE
255     II=1
	DO 275 I=1,N
	HEE=B(II)
	BB=DSQRT(HEE)
	DO 270 K=1,N
270     X(K,I)=X(K,I)/BB
275     II=II+N1-I
	RETURN
280     DO 290 I= 1,N
290     D(I)=EIGV(I)
	IF(NSWEEP.LT.NSMAX)GO TO 40
	GO TO 255
2000    FORMAT(3X,' SWEEP NO. IN JABI ',I4)
2010    FORMAT(3X,6(E19.12,1X))
2020    FORMAT(3X,'ERROR SOLUTION STOP ',I4,' A ',E20.12,' B ',E20.12)
2030    FORMAT(3X,' CURRENT EIGEN VALUES IN JACOBI ARE ')
	END


*****************************************************************

      SUBROUTINE PASOLV(SK,P,NDS,NN,NEQ1,NSKY,inde )
      implicit double precision(a-h,o-z)
      DIMENSION SK(NSKY),P(NN),NDS(NEQ1)
      DO 140 N=1,NN
      KN=ndS(N)
      KL=KN+1
      KU=ndS(N+1)-1
      KH=KU-KL
      IF(KH) 110,90,50
50    K=N-KH
      IC=0
      KLT=KU
      DO 80 J=1,KH
      IC=IC+1
      KLT=KLT-1
      KI=ndS(K)
      nd=ndS(K+1)-KI-1
      IF(nd)80,80,60
60    KK=MIN0(IC,nd)
      C=0.0
      DO 70 L=1,KK
70    C=C+SK(KI+L)*SK(KLT+L)
      SK(KLT)=SK(KLT)-C
80    K=K+1
90    K=N
      B=0.0
      DO 100 KK=KL,KU
      K=K-1
      KI=ndS(K)
      C=SK(KK)/SK(KI)
      B=B+C*SK(KK)
100   SK(KK)=C
      SK(KN)=SK(KN)-B
c110   IF(SK(KN))120,120,140
110   if(sk(kn) .gt. -0.001) goto 140      
120   WRITE(15,222)N,SK(KN),KN
      inde=1
      return
c     STOP
140   CONTINUE
C     RETURN
C     REDUCE RIGHT HAndSIDELOADVECTOR
150   DO 180 N=1,NN
      KL=ndS(N)+1
      KU=ndS(N+1)-1
      IF(KU-KL)180,160,160
160   K=N
      C=0.0
      DO 170 KK=KL,KU
      K=K-1
170   C=C+SK(KK)*P(K)
      P(N)=P(N)-C
180   CONTINUE
C     BACK SUBSTITUTION
      DO 200 N=1,NN
      K=ndS(N)
200   P(N)=P(N)/SK(K)
      IF(NN .EQ. 1)RETURN
      N=NN
      DO 230 L=2,NN
      KL=ndS(N)+1
      KU=ndS(N+1)-1
      IF(KU-KL)230,210,210
210   K=N
      DO 220 KK=KL,KU
      K=K-1
220   P(K)=P(K)-SK(KK)*P(N)
230   N=N-1
      RETURN
222   FORMAT(//20X,'STOP-STIFFNESS MATRIX NOT POSITIVE
     1DEFINITE',' NONPOSITIVE PIVOT FOR EQUATION',I4,//
     110X,'PIVOT=',E20.12)
      END


