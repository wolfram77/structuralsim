clear all
clc
ndcxn=[1 2;1 3;1 4];
xycod=[450 0;1250 600;900 600;0 600];
nelem=size(ndcxn,1);
nnode=2;
ndofn=2;
nodes=4;
AE=250*200*10^3;
K=zeros(nodes*ndofn);
a=0;
kel=zeros(12,4);
BLel=zeros(1,12);
for ielem=1:nelem
    a=a+1;
    if a==1
        b=[1 2 3 4];
    elseif a==2
        b=[5 6 7 8];
    elseif a==3
        b=[9 10 11 12];
 end
    node1=ndcxn(ielem,1);
    node2=ndcxn(ielem,2);
xcod1=xycod(node1,1);
    xcod2=xycod(node2,1);
    ycod1=xycod(node1,2);
    ycod2=xycod(node2,2);
    L=sqrt((xcod2-xcod1)^2+(ycod2-ycod1)^2);
    c=(xcod2-xcod1)/L;
    s=(ycod2-ycod1)/L;
    T=[c s 0 0;0 0 c s];
    k1=AE/L*[1 -1;1 -1];
    k=T'*k1*T;
     kel(b,1:4)=k;
     BLel(1,b)=[-c -s c s]/L;
    gbdof=[];
    for inode=1:nnode
        for idofn=1:ndofn
            gbdof=[gbdof (ndcxn(ielem,inode)-1)*ndofn+idofn];
        end
    end
    K(gbdof,gbdof)=K(gbdof,gbdof)+k;
end
rsdof=[3 4 5 6 7 8];
acdof=setdiff(1:8,rsdof);
f=[0;-18000;0;0;0;0;0;0];u=K(acdof,acdof)\f(acdof);
U=zeros(nodes*ndofn,1);
U(acdof)=u
F=K*U
kel;
f1=AE*BLel(1,1:4)*U(1:4,1)
d=[1 2 5 6];
f2=AE*BLel(1,5:8)*U(d,1)
f3=AE*BLel(1,9:12)*U(3:6,1)
