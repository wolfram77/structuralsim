function y = BeamElementStiffness (E,I,L)
%BeamElementStiffness     this function returns the element
%                         stiffness matrix for a beam element 
%                         with E,I and L. The Size of Matrix is 4 X 4
y = E*I/(L*L*L) * [12 6*L -12 6*L ; 6*L 4*L*L -6*L 2*L*L ; -12 -6*L 12-6*L; 6*L 2*L*L -6*L 4*L*L];

function Y = BeamElementForces(k,u,w)
%BeamElementForces    This function returns the element nodal force vector
%                     given the element stiffness matrix k and the element %                     nodal displacement vector u.
Y = (k * u)-w;
function Y = BeamElementShearDiagram (f,L)
%BeamElementShearDiagram       This function plots the shear force diagram
%                              for the beam element with nodal force f and %                              length L
x = [0 ; L];
z = [f(1); -f(3)];
hold on;
title('Shear Force Diagram');
plot (x,z);
Y1=[0;0];
plot(x,Y1,'k')
function Y = BeamElementMomentDiagram (f,L)
%BeamElementShearDiagram       This function plots the bending moment 
%                              diagram for the beam element with nodal 
%                              force f and length L
x = [0 ; L];
z = [-f(2); f(4)];
hold on;
title('Bending Moment Diagram');
plot (x,z);
Y1=[0;0];
plot(x,Y1,'k')

E = 2e8;

I1=1.8e-4;


I2=1.2e-4;


L1=6;

L2=4;

L3=1;

k1=BeamElementStiffness (E,I1,L1);

k2=BeamElementStiffness (E,I2,L2);

K1=zeros(6,6);

K1([1:4],1:4)=k1([1:4],1:4);

K2=zeros(6,6);

K2([3:6],3:6)=k2([1:4],1:4);

K=K1+K2;

f1=-90;

f2=-90;

f3=-120;

f4=60;

f5=-50;

f6=10;

F=[f1;f2;f3;f4;f5;f6];

F1=[f4;f6];

K3=[K(4,4),K(4,6);K(6,4),K(6,6)];

u1=0;

u2=0;

u3=0;

u5=0;

u7=(K3)\F1;

U=[u1;u2;u3;u7(1:1);u5;u7(2:2)];

U1=[U(1);U(2);U(3);U(4)];

U2=[U(3);U(4);U(5);U(6)];

f1=BeamElementForces(k1,U1);

f2=BeamElementForces(k2,U2);

w1=[-90;-90;-90;90];

w2=[-30;-30;-30;30];

f1=BeamElementForces(k1,U1,w1);

f2=BeamElementForces(k2,U2,w2);

F=[f1(1);f1(2);f1(3)+f2(1);f1(4)+f2(2);f2(3);f2(4)];

 BeamElementShearDiagram(f1,L1)
